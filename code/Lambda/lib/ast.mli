(** The main type for our AST (дерева абстрактного синтаксиса) *)
type ('b, 'name) t =
  | Var of 'name (** Variable [x] *)
  | Abs of 'b * ('b, 'name) t (** Abstraction [λx.t] *)
  | App of ('b, 'name) t * ('b, 'name) t (** Application [f x] *)

type named = (string, string) t
type nameless = (unit, int) t

type ('b, 'name) strat =
  { on_var : ('b, 'name) strat -> 'name -> ('b, 'name) t
  ; on_abs : ('b, 'name) strat -> 'name -> ('b, 'name) t -> ('b, 'name) t
  ; on_app : ('b, 'name) strat -> ('b, 'name) t -> ('b, 'name) t -> ('b, 'name) t
  }
