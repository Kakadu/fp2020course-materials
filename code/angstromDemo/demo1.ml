(** Copyright 2021-2023, Kakadu *)

(** SPDX-License-Identifier: LGPL-3.0-or-later *)

open AngstromDemo
open Angstrom

let () = assert (parse_string ~consume:Consume.All sum "0*0+0*0" = Ok "0*0+0*0")
