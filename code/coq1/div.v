Fail Definition foo : Set :=
forall A : Set, A -> A.

Definition foo : Type := forall A : Type, A -> A.

Print foo.


(* https://www.irif.fr/~letouzey/download/letouzey_extr_cie08.pdf *)

Section Well_founded.

  Variable A: Set.
  Variable R: A -> A -> Prop. (* predecessor *)
  Inductive Acc : A -> Prop :=
    Acc_intro : forall x: A,
      (forall y: A, R y x -> Acc y) ->
      Acc x.
  Definition Well_founded := forall a: A, Acc a.


  Definition Acc_inv: forall x: A,
    Acc x -> forall y: A, R y x -> Acc y :=
      fun x a =>
        match a in Acc x
              return forall y: A, R y x -> Acc y
        with | Acc_intro x2 f => f end.

  (* Заклинание можно не читать
    `in Acc x return forall y: A, R y x -> Acc y`
    Оно надо для проверки/вывода типов.
  *)

  Section Acc_iter.
    Variable P: A -> Type.
    Variable F: forall x,
      (forall y, R y x -> P y) -> P x.
    Fixpoint Acc_iter (x: A) (a: Acc x) { struct a} : P x :=
        F x (fun y h => Acc_iter y (Acc_inv x a y h)).
  End Acc_iter.
End Well_founded.

Check well_founded_induction.

Require Import Coq.Arith.Wf_nat.
Require Import Coq.Arith.Compare_dec.
Require Import ZArith.


Definition div : forall a b, b <> 0 ->
  { q | q*b <= a < (S q) * b }.
Proof.
  intro a; pattern a. apply (well_founded_induction lt_wf); clear a.
  intros a Hrec b Hb.
  elim (le_lt_dec b a); intros Hab.

  assert (H : a-b < a). omega .
  elim (Hrc (a-b) H b Hb); simpl; intros q (Hq, Hq').
  exists (S q); simpl; omega.
  exists O; omega.

(*
induction x as [x Hrec] using (well_founded_induction lt_wf).
intros y Hy.
destruct (y <=? x) as [Hyx|Hyx]. (* do we have y≤x or x<y ? *)
(* first case: y≤x *)
assert (Hxy : x-y < x) by omega.
destruct (Hrec (x-y) Hxy y Hy) as [z Hz]. (* ie: let z = div (x-y) y *)
exists (S z); simpl in *; omega. (* ie: z+1 fits as (div x y) *)
(* second case: x<y *)
exists 0; omega.
*)
Defined.
