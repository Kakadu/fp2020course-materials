let rec foldr f init = function [] -> init | h :: tl -> f h (foldr f init tl)

let%expect_test _ =
  Printf.printf "%d\n" (foldr ( * ) 1 [ 1; 2; 3; 4 ]);
  [%expect "24"]

let rec foldr1 f init = function
  | [] -> init
  | h :: tl -> f h (foldr1 f init tl) (fun x -> x)

let%expect_test _ =
  Printf.printf "%d\n" (foldr1 (fun a b k -> k (a * b)) 1 [ 1; 2; 3; 4 ]);
  [%expect "24"]

let rec foldl_sc f init = function
  | [] -> init
  | h :: tl -> f h init (fun acc -> foldl_sc f acc tl)

let%expect_test _ =
  let f a b k =
    if a = 0 || b = 0 then 0
    else
      let () = Printf.printf "%d * %d\n" a b in
      k (a * b)
  in
  Printf.printf "%d\n" (foldl_sc f 1 [ 1; 2; 3; 4 ]);
  [%expect "
    1 * 1
    2 * 1
    3 * 2
    4 * 6
    24"];
  Printf.printf "%d\n" (foldl_sc f 1 [ 1; 2; 0; 4 ]);
  [%expect "
    1 * 1
    2 * 1
    0"]

let rec foldr_cps f init k = function
  | [] -> k init
  | h :: tl -> foldr_cps f init (fun a -> f h a k) tl

let%expect_test _ =
  Printf.printf "%d\n"
    (foldr_cps (fun a b k -> k (a * b)) 1 Fun.id [ 1; 2; 3; 4 ]);
  [%expect "24"]

let%expect_test _ =
  Printf.printf "%d\n"
    (foldr_cps
       (fun a b k ->
         Printf.printf "a = %d, b=%d\n" a b;
         k (a * b))
       1 Fun.id [ 1; 2; 3; 4 ]);
  [%expect
    {|
      a = 4, b=1
      a = 3, b=4
      a = 2, b=12
      a = 1, b=24
      24|}]

let%expect_test _ =
  Printf.printf "%d\n"
    (foldr_cps
       (fun a b k ->
         Printf.printf "a = %d, b=%d\n" a b;
         if a = 0 then k 0 else k (a * b))
       1 Fun.id [ 1; 2; 0; 4 ]);
  [%expect
    {|
      a = 4, b=1
      a = 0, b=4
      a = 2, b=0
      a = 1, b=0
      0|}]
