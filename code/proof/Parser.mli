[@@@ocaml.text "/*"]

(** Copyright 2021-2024, Kakadu *)

(** SPDX-License-Identifier: LGPL-3.0-or-later *)

[@@@ocaml.text "/*"]

type assoc =
  | Left
  | Right

open Angstrom

val prio : 'a t -> ('b t * ('a -> 'a -> 'a)) list array -> 'a t
val prio_assoc : 'a t -> (assoc * 'b t * ('a -> 'a -> 'a)) list array -> 'a t

module Prop : sig
  type bin_op =
    | Conj
    | Disj
    | Impl
    | Equiv
  [@@deriving show]

  type expr =
    | Var of char
    | Neg of expr
    | Binop of bin_op * expr * expr
  [@@deriving show]
end

module User_cmd : sig
  type t =
    | Focus of int
    | Qed
    | Intros
    | Axiom
  [@@deriving show]
end

val parse_formula : string -> (Prop.expr, string) result
val parse_cmd : string -> (User_cmd.t, string) result
