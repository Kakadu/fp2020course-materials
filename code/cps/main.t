We test in bytecode because StackOverflow detection is not reliable
https://discuss.ocaml.org/t/stack-overflow-reported-as-segfault/8646/6
  $ ./cps_main.bc
  lists
  depth = 262074, size_tail = 262074
  depth = 262075, size_tail = <stack overflow>
  non-tailrec trees
  depth = 174716, size_tail = 274715
  depth = 174717, size_tail = <stack overflow>
  Should work correctly twice
  depth = 174716, size_tail = 274715
  depth = 174717, size_tail = 274716
  $ ./cps_main.exe
  Getting Stack_overflow exception in native code is not reliable

  $ ./demo_monad.exe
  Size vs size-CPS-monadic
  depth = 174716, size_tail = 274715
  depth = 174717, size_tail = 274716
  depth = 174717, size_tail = 274716
