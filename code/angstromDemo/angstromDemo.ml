(** Copyright 2021-2023, Kakadu *)

(** SPDX-License-Identifier: LGPL-3.0-or-later *)

open Angstrom

let rec fix_comb f a = f (fix_comb f) a

(* http://okmij.org/ftp/Computation/fixed-point-combinators.html#Poly-variadic *)
let fix_poly (* (('a -> 'b) list -> 'a -> 'b) list -> ('a -> 'b) list  *) l =
  fix_comb (fun self l -> List.map (fun li eta -> li (self l) eta) l) l

[@@@warning "-8-27"]

let [ even; odd ] =
  let open_even [ even; odd ] n = n = 0 || odd (n - 1)
  and open_odd [ even; odd ] n = n <> 0 && even (n - 1) in
  fix_poly [ open_even; open_odd ]

[@@@warning "+8+27"]

let%test _ = even 10
let%test _ = odd 11

let parens p =
  char '(' >>= fun _ ->
  p >>= fun x ->
  char ')' >>= fun _ -> return x

let const = char '0' >>= fun c -> return (Printf.sprintf "%c" c)

let rec product sum =
  choice
    [
      parens sum;
      ( const >>= fun h ->
        many (char '*' >>= fun _ -> product sum) >>= fun tl ->
        return (List.fold_left (fun acc r -> Printf.sprintf "%s*%s" acc r) h tl)
      );
    ]

let rec sum product =
  choice
    [
      parens (sum product);
      ( product >>= fun h ->
        many (char '+' >>= fun _ -> product) >>= fun tl ->
        return (List.fold_left (fun acc r -> Printf.sprintf "%s+%s" acc r) h tl)
      );
    ]

[@@@warning "-8-27"]

(*
type arith_ = Sum | Product

let arith =
  fix (fun self -> function
    | Product ->
        choice
          [
            parens (self Sum);
            ( const >>= fun h ->
              many (char '*' >>= fun _ -> self Product)
              >>= fun tl ->
              return
                (List.fold_left
                   (fun acc r ->
                     Printf.sprintf "%s*%s" acc r)
                   h tl) );
          ]
    | Sum ->
        choice
          [
            parens (self sum);
            ( product >>= fun h ->
              many (char '+' >>= fun _ -> self Product)
              >>= fun tl ->
              return
                (List.fold_left
                   (fun acc r ->
                     Printf.sprintf "%s+%s" acc r)
                   h tl) );
          ]) *)

(* let s, p =
     let rec s = lazy (sum (Lazy.force p))
     and p = lazy (product (Lazy.force s)) in
     (s, p)

   let%test _ =
     parse_string ~consume:Consume.All (Lazy.force s) "0*0+0*0"
     = Ok "0*0+0*0" *)

(* This is a dirty hack for the case when I will preperly understand how to use angstrom *)

let sum, product =
  let sum1 : string t ref = ref @@ Obj.magic (fun _ -> failwith "sum") in
  let pro1 : string t ref = ref @@ Obj.magic (fun _ -> failwith "product") in
  let sum =
    fix @@ fun self ->
    choice
      [
        parens self;
        ( !pro1 >>= fun h ->
          many (char '+' >>= fun _ -> !pro1) >>= fun tl ->
          return
            (List.fold_left (fun acc r -> Printf.sprintf "%s+%s" acc r) h tl) );
      ]
  in
  let product =
    fix @@ fun self ->
    choice
      [
        parens !sum1;
        ( const >>= fun h ->
          many (char '*' >>= fun _ -> self) >>= fun tl ->
          return
            (List.fold_left (fun acc r -> Printf.sprintf "%s*%s" acc r) h tl) );
      ]
  in
  pro1 := product;
  sum1 := sum;
  (sum, product)

let%test _ = parse_string ~consume:Consume.All sum "0*0+0*0" = Ok "0*0+0*0"

module Workaround2 = struct
  type dispatch = { sum : dispatch -> string t; product : dispatch -> string t }

  let d =
    let sum d =
      fix @@ fun self ->
      choice
        [
          parens self;
          ( d.product d >>= fun h ->
            many (char '+' >>= fun _ -> d.product d) >>= fun tl ->
            return
              (List.fold_left (fun acc r -> Printf.sprintf "%s+%s" acc r) h tl)
          );
        ]
    in
    let product d =
      fix @@ fun self ->
      choice
        [
          parens (d.sum d);
          ( const >>= fun h ->
            many (char '*' >>= fun _ -> self) >>= fun tl ->
            return
              (List.fold_left (fun acc r -> Printf.sprintf "%s*%s" acc r) h tl)
          );
        ]
    in
    { sum; product }

  let%test _ =
    parse_string ~consume:Consume.All (d.sum d) "0*0+0*0" = Ok "0*0+0*0"
end

(*
let [ sum; product ] =
  let open_sum [ sum; product ] =
    choice
      [
        parens sum;
        ( product >>= fun h ->
          many (char '+' >>= fun _ -> product) >>= fun tl ->
          return
            (List.fold_left
               (fun acc r -> Printf.sprintf "%s+%s" acc r)
               h tl) );
      ]
  and open_product [ sum; product ] =
    choice
      [
        parens sum;
        ( const >>= fun h ->
          many (char '*' >>= fun _ -> product) >>= fun tl ->
          return
            (List.fold_left
               (fun acc r -> Printf.sprintf "%s*%s" acc r)
               h tl) );
      ]
  in

  fix_poly [ open_sum; open_product ]
*)
(* let rec s = lazy (sum (Lazy.force p))
   and p = lazy (product (Lazy.force s)) in
   [ s; p ] *)

(*
81 |   fix_poly [ open_sum; open_product ]
                  ^^^^^^^^
Error: This expression has type string t list -> string t
       but an expression was expected of type ('a -> 'b) list -> 'a -> 'b
       Type string t is not compatible with type 'a -> 'b  *)

[@@@warning "+8+27"]

(* let fix_poly2
       (* (('a -> 'b) list -> 'a -> 'b) list -> ('a -> 'b) list  *)
         (a, b) =
     fix_comb
       (fun self (a, b) ->
         ( (fun eta -> a (self (a, b)) eta),
           fun eta -> b (self (a, b)) eta ))
       (a, b)

   let a, b = fix_poly2 (sum, product) *)
