[@@@ocaml.text "/*"]

(** Copyright 2021-2024, Kakadu *)

(** SPDX-License-Identifier: LGPL-3.0-or-later *)

[@@@ocaml.text "/*"]

open Lib_exprs

module TestQCheck = struct
  open Expr

  let digit = QCheck.Gen.int_range 0 9

  let expr_gen =
    QCheck.Gen.(
      sized
      @@ fix (fun self n ->
             (* Printf.printf "Generate of size %d\n%!" n; *)
             if n <= 1 then map const digit
             else
               frequency
                 [
                   (1, map2 (binop Plus) (self (n - 1)) (self (n - 1)));
                   (1, map2 (binop Dash) (self (n - 1)) (self (n - 1)));
                   (1, map2 (binop Asterisk) (self (n - 1)) (self (n - 1)));
                   (1, map2 (binop Slash) (self (n - 1)) (self (n - 1)));
                 ]))

  let arbitrary_lam =
    let open QCheck.Iter in
    let shrink_lam = function
      | Const i -> QCheck.Shrink.int i >|= const
      | Binop (_op, l, r) -> of_list [ l; r ]
    in
    QCheck.make expr_gen ~print:(Format.asprintf "%a" pretty) ~shrink:shrink_lam

  let print_parse_is_identity =
    QCheck.(
      Test.make arbitrary_lam (fun l ->
          Result.ok l
          = Parser_lib.parse_string expr (Format.asprintf "%a" pretty l)))

  let run () = QCheck_runner.run_tests ~long:false [ print_parse_is_identity ]
end

let __ () = Format.printf "Failed tests: %d\n%!" (TestQCheck.run ())

let () =
  let open Parser_lib in
  let open Lib_exprs in
  let input = String.init 9001 (fun n -> if n mod 2 <> 0 then '*' else '1') in
  (* print_endline input;  *)
  test Expr.expr (fun ppf _ -> Format.fprintf ppf "parsable") input

let () =
  let open Parser_lib in
  let open Lib_exprs in
  let input = String.init 9001 (fun n -> if n mod 2 <> 0 then '*' else '1') in
  (* print_endline input;  *)
  test Expr.expr (fun ppf _ -> Format.fprintf ppf "parsable") input

include struct
  let f x = if x then ( + ) 1 else ( * ) 2
  let fk x k = if x then k (fun a -> a + 1) else k (fun a -> a * 2)
  let f2 x y = f x y
  let f2k x y k = fk x (fun fa -> k (fa y))
  let _1 = fk true
  let _2 = f2k true 1
  let _3 x y = f2k x y Fun.id
end
