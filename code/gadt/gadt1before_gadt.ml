type ('a, 'b) eq

let cast : ('a, 'b) eq -> 'a -> 'b = fun _ -> Obj.magic
let symm : ('a, 'b) eq -> ('b, 'a) eq = Sys.opaque_identity

let trans : ('a, 'b) eq -> ('b, 'c) eq -> ('a, 'c) eq =
 fun _ -> Sys.opaque_identity

module Length : sig
  type t

  val of_int : int -> t
  val eq_int : (t, int) eq
end = struct
  type t = int

  let of_int x = if x < 0 then failwith "bad argument" else x
  let eq_int : (t, int) eq = Obj.magic 1
end

let print_length : Length.t -> unit =
 fun len -> cast Length.eq_int len |> print_int

let%expect_test _ =
  print_length (Length.of_int 123);
  [%expect {| 123 |}]
