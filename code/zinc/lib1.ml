(*
https://xavierleroy.org/mpri/2-4/machines.pdf
https://github.com/kseo/secd/blob/master/src/SECD/Eval.hs
https://xavierleroy.org/talks/zam-kazam05.pdf
*)

module SECD = struct
  type instr =
    | Access of int
    | Let
    | EndLet
    | Closure of instr list
    | Apply
    | Return
    (* basic stuff   *)
    | Const of int
    | Add
    | Mul
  [@@deriving show { with_path = false }]

  let pp_code ppf =
    Format.(pp_print_list ~pp_sep:(fun ppf () -> fprintf ppf "; ") pp_instr) ppf

  module Env = struct
    let get n xs = List.nth xs (n - 1)
    let push x xs = x :: xs
  end

  type env = stack_item list

  and stack_item =
    | VConst of int
    | VClosure of instr list * env
    | Pending of instr list
  [@@deriving show { with_path = false }]

  let rec eval1 ins env stack =
    match (ins, env, stack) with
    | Access n :: c, e, s -> (c, env, Env.get n e :: s)
    | Let :: c, e, v :: s -> (c, Env.push v e, s)
    | EndLet :: c, _ :: e, s -> (c, e, s)
    | Closure code :: c, e, s -> (c, e, VClosure (code, e) :: s)
    | Apply :: c, e, v :: VClosure (code, e') :: s ->
        (code, v :: e', VClosure (c, e) :: s)
    | Return :: _c, _e, v :: VClosure (c', e') :: s -> (c', e', v :: s)
    (* basic stuff *)
    | Const n :: c, env, s -> (c, env, VConst n :: s)
    | Add :: c, env, VConst n2 :: VConst n1 :: s ->
        (c, env, VConst (n1 + n2) :: s)
    | Mul :: c, env, VConst n2 :: VConst n1 :: s ->
        (c, env, VConst (n1 * n2) :: s)
    | _ -> failwith "Compiled badly"

  and eval iss env stack =
    match iss with
    | [] -> (env, stack)
    | is ->
        let code, env, stack = eval1 is env stack in
        eval code env stack

  let rec compile = function
    | AST.Var n -> [ Access n ]
    | Const n -> [ Const n ]
    | Abs a -> [ Closure (compile a @ [ Return ]) ]
    | App (a, b) -> compile a @ compile b @ (* fixme *) [ Apply ]
    | Let (a, b) -> compile a @ [ Let ] @ compile b @ [ EndLet ]
    | Binop (Add, a1, a2) -> compile a1 @ compile a2 @ [ Add ]
    | Binop (Mul, a1, a2) -> compile a1 @ compile a2 @ [ Mul ]
end

let%expect_test "compilation example" =
  let open SECD in
  let code = compile AST.(App (Abs (Binop (Add, Var 1, Const 1)), Const 2)) in
  Format.printf "code: %a\n%!" pp_code code;

  [%expect "code: Closure ([Access (1); Const (1); Add; Return]); Const (2); Apply"]

let%expect_test "Execution example" =
  let open SECD in
  let code = [ Closure [ Access 1; Const 1; Add; Return ]; Const 2; Apply ] in
  let trace code env stack =
    Format.printf "\nTICK\n%!";
    Format.printf "code: %a\n%!" pp_code code;
    Format.printf "env: %a\n%!" pp_env env;
    Format.printf "stack: %a\n%!" pp_env stack
  in

  let rec loop code env stack =
    trace code env stack;
    match code with
    | [] -> Format.printf "Fin\n%!"
    | _ ->
        let code, env, stack = eval1 code env stack in
        loop code env stack
  in
  loop code [] [];
  [%expect "
    TICK
    code: Closure ([Access (1); Const (1); Add; Return]); Const (2); Apply
    env: []
    stack: []

    TICK
    code: Const (2); Apply
    env: []
    stack: [VClosure ([Access (1); Const (1); Add; Return], [])]

    TICK
    code: Apply
    env: []
    stack: [VConst (2); VClosure ([Access (1); Const (1); Add; Return], [])]

    TICK
    code: Access (1); Const (1); Add; Return
    env: [VConst (2)]
    stack: [VClosure ([], [])]

    TICK
    code: Const (1); Add; Return
    env: [VConst (2)]
    stack: [VConst (2); VClosure ([], [])]

    TICK
    code: Add; Return
    env: [VConst (2)]
    stack: [VConst (1); VConst (2); VClosure ([], [])]

    TICK
    code: Return
    env: [VConst (2)]
    stack: [VConst (3); VClosure ([], [])]

    TICK
    code:
    env: []
    stack: [VConst (3)]
    Fin"]
