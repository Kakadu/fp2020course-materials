
type 'a option =
| Some of 'a
| None

type ('a, 'b) prod =
| Pair of 'a * 'b



val ltb : int -> int -> bool

type key = int

type color =
| Red
| Black

type 'a tree =
| E
| T of color * 'a tree * key * 'a * 'a tree

val empty_tree : 'a1 tree

val lookup : key -> 'a1 option tree -> 'a1 option

val balance :
  color -> 'a1 tree -> key -> 'a1 -> 'a1 tree -> 'a1 tree

val ins : key -> 'a1 -> 'a1 tree -> 'a1 tree

val make_black : 'a1 tree -> 'a1 tree

val insert : key -> 'a1 -> 'a1 tree -> 'a1 tree

val elements_tr :
  'a1 tree -> (key, 'a1) prod list -> (key, 'a1) prod list

val elements : 'a1 tree -> (key, 'a1) prod list
