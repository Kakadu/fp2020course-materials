module type MONAD = sig
  type 'a t

  val return : 'a -> 'a t
  val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t
  val ( let* ) : 'a t -> ('a -> 'b t) -> 'b t
end

module type MONADERROR = sig
  include MONAD

  val error : string -> 'a t
end

module Cont : sig
  include MONADERROR

  val run_cont : ('a -> 'b) -> 'a t -> 'b
end = struct
  type ('a, 'b) cont = 'a -> 'b
  type 'a t = { cont : 'b. ('a, 'b) cont -> 'b }

  let return (x : 'a) = { cont = (fun k -> k x) }

  let ( >>= ) (x : 'a t) (f : 'a -> 'b t) : 'b t =
    { cont = (fun k -> x.cont (fun v -> (f v).cont k)) }

  let ( let* ) = ( >>= )
  let error = failwith
  let run_cont f { cont } = cont f
end

type tree = Leaf | Node of tree * tree

let rec make depth =
  if depth <= 0 then Leaf
  else
    let r = make (depth - 1) in
    let l = if depth mod 100000 = 0 then r else Leaf in
    Node (l, r)

let size root =
  let rec helper tree =
    match tree with Leaf -> 0 | Node (l, r) -> helper l + helper r + 1
  in
  helper root

let make_tail depth =
  let rec helper acc n =
    if depth < n then acc
    else
      let l = if n mod 100000 = 0 then acc else Leaf in
      helper (Node (l, acc)) (n + 1)
  in
  helper Leaf 1

(* CPS transformation *)
let size_tail root =
  let rec helper tree k =
    match tree with
    | Leaf -> k 0
    | Node (l, r) ->
        (helper [@tailcall]) l (fun sl ->
            (helper [@tailcall]) r (fun sr -> k (sl + sr + 1)))
  in
  helper root (fun n -> n)

let make_cpsm depth =
  let rec helper acc n =
    if depth < n then acc
    else
      let l = if n mod 100000 = 0 then acc else Leaf in
      helper (Node (l, acc)) (n + 1)
  in
  helper Leaf 1

let size_cpsm root =
  let open Cont in
  let rec helper = function
    | Leaf -> return 0
    | Node (l, r) ->
        let* l = helper l in
        let* r = helper r in
        return (l + r + 1)
  in
  run_cont (fun n -> n) (helper root)

let wrap f n =
  Format.printf "depth = %d, size_tail = %s\n%!" n
    (try f n with Stack_overflow -> "<stack overflow>")

let () =
  print_endline "Size vs size-CPS-monadic";
  let depth = 174_716 in
  wrap (fun n -> string_of_int @@ size (make_tail n)) depth;
  wrap (fun n -> string_of_int @@ size_cpsm (make_tail n)) (depth + 1);
  ()

include struct
  let return_ x k = k x
  let run = return_
  let ( let** ) x f : _ = fun k -> x (fun v -> (f v) k)

  let size_cpsm2 root =
    let rec helper = function
      | Leaf -> return_ 0
      | Node (l, r) ->
          let** l = helper l in
          let** r = helper r in
          return_ (l + r + 1)
    in
    run (fun n -> n) (helper root)

  let () =
    let depth = 174_716 in
    wrap (fun n -> string_of_int @@ size_cpsm2 (make_tail n)) (depth + 1);
    ()
end
