[@@@ocaml.text "/*"]

(** Copyright 2021-2024, Kakadu *)

(** SPDX-License-Identifier: LGPL-3.0-or-later *)

[@@@ocaml.text "/*"]

[@@@ocaml.warnerror "-32"]

open Parser_lib

type assoc = Left | Right

let prio expr table =
  let len = Array.length table in
  let rec helper level =
    if level >= len then expr
    else
      let xs = table.(level) in
      return (List.fold_left (fun acc (op, r) -> op acc r))
      <*> helper (level + 1)
      <*> many
            (choice
               (List.map
                  (fun (op, f) ->
                    op *> helper (level + 1) >>= fun r -> return (f, r))
                  xs))
  in
  helper 0

let prio_assoc expr table =
  let len = Array.length table in
  let rec loop acc = function
    | [] -> acc
    | (Left, op, r) :: tl -> loop (op acc r) tl
    | (Right, op, r) :: tl -> op acc (loop r tl)
  in
  let rec helper level =
    if level >= len then expr
    else
      let xs = table.(level) in
      return loop
      <*> helper (level + 1)
      <*> many
            (choice
               (List.map
                  (fun (ass, op, f) ->
                    op *> helper (level + 1) >>= fun r -> return (ass, f, r))
                  xs))
  in
  helper 0

module Expr = struct
  type bin_op = Plus | Dash | Asterisk | Slash | Power
  [@@deriving show { with_path = false }]

  type expr = Const of int | Binop of bin_op * expr * expr
  [@@deriving show { with_path = false }]

  let const c = Const c
  let binop op a b = Binop (op, a, b)
  let expr_small = digit >>| const

  let expr =
    prio expr_small
      [|
        [
          (ws *> char '+' <* ws, fun a b -> Binop (Plus, a, b));
          (ws *> char '-' <* ws, fun a b -> Binop (Dash, a, b));
        ];
        [
          (ws *> char '*' <* ws, fun a b -> Binop (Asterisk, a, b));
          (ws *> char '/' <* ws, fun a b -> Binop (Slash, a, b));
        ];
      |]

  let%expect_test _ =
    test expr pp_expr "asdf";
    [%expect {| Error: '' |}]

  let%expect_test _ =
    test expr pp_expr "1+2";
    [%expect {| Binop (Plus, Const (1), Const (2)) |}]

  let%expect_test _ =
    test expr pp_expr "1+2*3";
    [%expect
      {| Binop (Plus, Const (1), Binop (Asterisk, Const (2), Const (3))) |}]

  open Format

  let rec pretty ppf = function
    | Const n -> fprintf ppf "%d" n
    | Binop (Plus, l, r) -> fprintf ppf "%a+%a" pretty l pretty r
    | Binop (Dash, l, r) -> fprintf ppf "%a-%a" pretty l pretty r
    | Binop (Asterisk, l, r) -> fprintf ppf "%a*%a" pretty l pretty r
    | Binop (Power, l, r) -> fprintf ppf "(%a**%a)" pretty l pretty r
    | Binop (Slash, l, r) -> fprintf ppf "%a/%a" pretty l pretty r

  let%expect_test _ =
    test expr pretty "1+2*3";
    [%expect {| 1+2*3 |}]

  let%expect_test _ =
    test expr pretty "0+0*0";
    [%expect {| 0+0*0 |}]

  let%expect_test _ =
    test expr pretty "0*0*0";
    [%expect {| 0*0*0 |}]

  let%expect_test _ =
    let input =
      String.init 900001 (fun n -> if n mod 2 <> 0 then '*' else '1')
    in
    test expr (fun ppf _ -> Format.fprintf ppf "parsable") input;
    [%expect {| parsable |}]

  let expr2 =
    prio_assoc expr_small
      [|
        [
          (Left, ws *> char '+' <* ws, fun a b -> Binop (Plus, a, b));
          (Left, char '-', fun a b -> Binop (Dash, a, b));
        ];
        [ (Left, ws *> char '/' <* ws, fun a b -> Binop (Slash, a, b)) ];
        [
          ( Right,
            ws *> char '*' *> char '*' <* ws,
            fun a b -> Binop (Power, a, b) );
        ];
      |]

  let%expect_test _ =
    test expr2 pretty "1/2+1**2**3";
    [%expect {|
      1/2+(1**(2**3)) |}]
end
