(* Rewrite of 
   https://github.com/mayconamaro/drinkerParadox/blob/master/drinkerParadox.v *)
Definition XM : Prop := forall X : Prop, X \/ not X.

Print True.

Lemma drinker (X : Type) (D : X -> Prop) :
  XM -> (exists x : X, True) -> exists x, (D x -> forall x, D x).
Proof.
  intros XM NonEmpty.
  specialize (XM (exists x : X, not (D x))) as H1.
  destruct H1 as [H2 | H3].
  * intros. destruct H2 as [z H3]. exists z. intros notH3. contradiction. 
  * destruct NonEmpty.
    exists x. intros.
    specialize (XM (D x0)) as XMD. destruct XMD.
    + assumption.
    + (* absurd W говорит, что мы готовимся продемонстрировать противоречие 
         в посылках путём демонстрации истинность W /\ not W *)
      absurd (exists x, ~ D x).
      ** assumption.
      ** exists x0. assumption.
Qed.