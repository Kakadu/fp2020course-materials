type rhs =
  | Term of string
  | NonTerm of string

let term s =
  assert (Char.lowercase_ascii s.[0] = s.[0]);
  Term s
;;

let nont s =
  assert (Char.uppercase_ascii s.[0] = s.[0]);
  NonTerm s
;;

type rule = string * rhs list
type gram = rule list

let rule l r = l, r

include struct
  open Format

  let pp_nont ppf s = pp_print_string ppf s

  let pp_rhs ppf = function
    | Term s -> Format.fprintf ppf "%s" s
    | NonTerm s -> Format.fprintf ppf "%s" s
  ;;

  let pp_rule ppf (lhs, rhss) =
    fprintf ppf "%a -> @[%a@]" pp_nont lhs (pp_print_list pp_rhs) rhss
  ;;

  let pp_gram ppf rs =
    fprintf ppf "@[<v>%a@]" (pp_print_list ~pp_sep:pp_print_space pp_rule) rs
  ;;
end

let gram1 : rule list =
  [ rule "S" [ term "a"; nont "B" ]
  ; rule "S" [ term "b"; nont "A" ]
  ; rule "A" [ term "a" ]
  ; rule "B" [ term "b" ]
  ; rule "A" [ term "b"; nont "A"; nont "A" ]
  ; rule "B" [ term "a"; nont "B"; nont "B" ]
  ; rule "A" [ term "a"; nont "S" ]
  ; rule "B" [ term "b"; nont "S" ]
  ]
;;

let lookup_rhs g nt =
  List.filter_map
    (function
      | a, rhs -> if a = nt then Some rhs else None)
    g
;;

let%expect_test " " =
  pp_gram Format.std_formatter gram1;
  [%expect
    {|
    S -> aB
    S -> bA
    A -> a
    B -> b
    A -> bAA
    B -> aBB
    A -> aS
    B -> bS |}]
;;

(* let deriv g rhs var =
   let ( >>= ) xs f = List.concat (List.map f xs) in
   let rec helper rhs =
   rhs
   >>= function
   | [] -> assert false
   | Term h :: tl when String.equal h var -> tl
   | Term _ :: tl -> assert false
   | NonTerm nt :: tl ->
   let rhss = lookup_rhs g nt in
   assert false
   in
   helper rhs
   ;; *)

let%expect_test " " =
  print_float Float.(max (-.nan) nan);
  [%expect {| -nan |}];
  print_float Float.(max nan (-.nan));
  [%expect {| -nan |}]
;;
