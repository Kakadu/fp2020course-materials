open Angstrom

let test p pp text =
  match Angstrom.parse_string ~consume:Angstrom.Consume.All p text with
  | Result.Ok ast -> Format.printf "%a\n%!" pp ast
  | Error s -> Format.printf "Error: %s" s
;;

let%expect_test _ =
  test (char 'a') (fun ppf _ -> Format.fprintf ppf "OK") "a";
  [%expect {| OK |}];
  test (char 'a') (fun ppf _ -> Format.fprintf ppf "OK") "ab";
  [%expect {| Error: : end_of_input |}]
;;

let%expect_test _ =
  test (char 'a' *> char 'b') Format.pp_print_char "a";
  [%expect {| Error: : not enough input |}];
  test (char 'a' *> char 'b') Format.pp_print_char "ab";
  [%expect {| b |}];
  test (char 'a' <* char 'b') Format.pp_print_char "ab";
  [%expect {| a |}]
;;

let digit =
  let open Angstrom in
  let* c = any_char in
  if c <= '9' && '0' <= c then return (Char.code c - Char.code '0') else fail "digit "
;;

let%expect_test _ =
  test digit Format.pp_print_int "2";
  [%expect {| 2 |}];
  test (return 8) Format.pp_print_int "";
  [%expect {| 8 |}];
  test (return ( + ) <*> digit <*> digit) Format.pp_print_int "23";
  [%expect {| 5 |}]
;;

let ws =
  let open Angstrom in
  many (char ' ' <|> char '\n')
;;

let%expect_test _ =
  test (return ( + ) <*> digit <*> digit) Format.pp_print_int " 2 3 ";
  [%expect {| Error: : digit |}]
;;

let%expect_test _ =
  let pair =
    let id x = x in
    id 1, id true
  in
  Format.printf "%d %b\n" (fst pair) (snd pair);
  [%expect {| 1 true |}]
;;

let%expect_test _ =
  let binop_parser =
    digit
    >>= fun l ->
    any_char
    >>= fun c ->
    digit
    >>= fun r ->
    match c with
    | '+' -> return (l + r)
    | '-' -> return (l - r)
    | '*' -> return (l * r)
    | '/' -> return (l / r)
    | _ -> fail "Bad operator"
  in
  test binop_parser Format.pp_print_int "2+3";
  [%expect {| 5 |}];
  test binop_parser Format.pp_print_int "2*3";
  [%expect {| 6 |}];
  test binop_parser Format.pp_print_int "2-3";
  [%expect {| -1 |}];
  test binop_parser Format.pp_print_int "8/2";
  [%expect {| 4 |}];
  let binop_parser2 =
    let ( let* ) x f = x >>= f in
    let* l = digit in
    let* c = any_char in
    let* r = digit in
    match c with
    | '+' -> return (l + r)
    | '-' -> return (l - r)
    | '*' -> return (l * r)
    | '/' -> return (l / r)
    | _ -> fail "Bad operator"
  in
  test binop_parser2 Format.pp_print_int "2+3";
  [%expect {| 5 |}];
  test binop_parser2 Format.pp_print_int "2*3";
  [%expect {| 6 |}];
  test binop_parser2 Format.pp_print_int "2-3";
  [%expect {| -1 |}];
  test binop_parser2 Format.pp_print_int "8/2";
  [%expect {| 4 |}];
  test binop_parser2 Format.pp_print_int "8.2";
  [%expect {| Error: : Bad operator |}];
  ()
;;
