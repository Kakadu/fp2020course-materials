(** Copyright 2021-2023, Kakadu *)

(** SPDX-License-Identifier: LGPL-3.0-or-later *)

type pattern = PVar of string [@@deriving show { with_path = false }]

let pvar s = PVar s

type rec_flag =
  | Recursive
  | NonRecursive
[@@deriving show { with_path = false }]

type expr =
  | EConst of int
  | EVar of string
  | EIf of expr * expr * expr
  | ELam of pattern * expr
  | EApp of expr * expr
  | ELet of rec_flag * pattern * expr * expr
[@@deriving show { with_path = false }]

let econst n = EConst n
let evar s = EVar s
let elam v body = ELam (v, body)

let eapp f = function
  | [] -> f
  | args -> List.fold_left (fun f x -> EApp (f, x)) f args
;;

let elet ?(isrec = NonRecursive) p b wher = ELet (isrec, p, b, wher)
let eite c t e = EIf (c, t, e)
let emul a b = eapp (evar "*") [ a; b ]
let eadd a b = eapp (evar "+") [ a; b ]
let esub a b = eapp (evar "-") [ a; b ]
let eeq a b = eapp (evar "=") [ a; b ]
