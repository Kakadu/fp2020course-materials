(** Copyright 2021-2023, Kakadu *)

(** SPDX-License-Identifier: LGPL-3.0-or-later *)

type error =
  [ `Occurs_check
  | `No_variable of string
  | `Unification_failed of Typedtree.ty * Typedtree.ty
  ]

(** Main function: Algorithm W *)
val w : Parsetree.expr -> (Typedtree.ty, error) Result.t

val pp_error : Format.formatter -> error -> unit
