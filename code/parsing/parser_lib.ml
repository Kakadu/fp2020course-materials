(** Copyright 2021-2024, Kakadu *)

(** SPDX-License-Identifier: LGPL-3.0-or-later *)

(** Библиотека парсер-комбинаторов от печки *)

type input = char list
type 'a parse_result = Failed | Parsed of 'a * input

(** В принципе тип parser_result эквивалентен следующему

  type 'a parse_result = ('a * input) option
*)

type 'a parser = input -> 'a parse_result
(** Так parser это type abbreviation (в терминах С -- typedef),
  то правая часть частенько видна при выводе типов. Формально компилятор прав,
  но типы получаются несколько длиннее, чем хотелось бы. Чтобы этого не было,
  можно описать этот тип немного по-другому и исправить реализацию

  type 'a parser = Parser of input -> 'a parse_result [@@unboxed]
*)

let pp_parse_result f ppf =
  let open Format in
  function
  | Failed -> fprintf ppf "fail"
  | Parsed (x, rest) ->
      fprintf ppf "Parsed (%a, \"%a\")" f x
        (pp_print_list ~pp_sep:pp_print_flush pp_print_char)
        rest

(* ***************** Простой парсер 1 ********************* *)
let fail : _ parser = fun _ -> Failed
let return x : _ parser = fun s -> Parsed (x, s)

let char c : _ parser = function
  | h :: tl when c = h -> return c tl
  | _ -> Failed

let rec list_map : 'a 'b. ((* forall *) 'a -> 'b) -> 'a list -> 'b list =
 fun f xs -> match xs with [] -> [] | h :: tl -> f h :: list_map f tl

let%test "my first test" = char 'a' [ 'a'; 'b'; 'c' ] = return 'a' [ 'b'; 'c' ]
let%test _ = char 'b' [ 'a'; 'b' ] = Failed

let%expect_test "test name" =
  pp_parse_result Format.pp_print_char Format.std_formatter
    (char 'a' [ 'a'; 'b'; 'c'; 'f' ]);
  [%expect {| Parsed (a, "bcf") |}];
  pp_parse_result Format.pp_print_char Format.std_formatter
    (char 'b' [ 'a'; 'b' ]);
  [%expect {| fail |}]

let satisfy cond : _ parser = function
  | h :: tl when cond h -> return h tl
  | _ -> Failed

let char c = satisfy (Char.equal c)

let digit_c =
  let is_digit = function '0' .. '9' -> true | _ -> false in
  satisfy is_digit

let%test _ = digit_c [ '0' ] = return '0' []
let%test _ = digit_c [ 'a' ] = Failed

(* ****************** Простой парсер  ********************************** *)
let ( <*> ) : 'a 'b. ('a -> 'b) parser -> 'a parser -> 'b parser =
 fun pf parg s ->
  match pf s with
  | Failed -> Failed
  | Parsed (f, tl) -> (
      match parg tl with
      | Failed -> Failed
      | Parsed (arg, tl) -> Parsed (f arg, tl))

let%test _ =
  let p1 = return (fun _ b -> b) <*> char 'a' <*> char 'b' in
  p1 [ 'a'; 'b' ] = return 'b' []

(* ****************** Простой парсер 2 ********************************** *)
(* Произносится bind (англ. [baɪnd), или 'a затем'.
   Встретится позже на паре про монады.
*)
let ( >>= ) : 'a 'b. 'a parser -> ('a -> 'b parser) -> 'b parser =
 fun p f s -> match p s with Failed -> Failed | Parsed (h, tl) -> f h tl

let%test _ =
  let p1 = digit_c >>= char in
  p1 [ '0'; '0' ] = return '0' []

let%test _ =
  let p1 = digit_c >>= char in
  p1 [ '0'; '1' ] = Failed

(* ******************** Простые комбинаторы ***************************** *)
let ( *> ) : 'a 'b. 'a parser -> 'b parser -> 'b parser =
 fun p1 p2 -> p1 >>= fun _ -> p2

let%test _ =
  let p1 = char 'a' *> char 'b' in
  p1 [ 'a'; 'b' ] = return 'b' []

let ( <* ) : 'a 'b. 'a parser -> 'b parser -> 'a parser =
 fun p1 p2 ->
  p1 >>= fun h ->
  p2 >>= fun _ -> return h

let%test _ =
  let p2 = char 'a' <* char 'b' in
  p2 [ 'a'; 'b' ] = return 'a' []

let digit = digit_c >>= fun c -> return (Char.code c - Char.code '0')
let%test _ = digit [ '9' ] = return 0o11 []

(* *************************************************************** *)
(* Комбинатор fmap нужен, чтобы сделать что-то с результатом парсера *)
let ( >>| ) : 'a 'b. 'a parser -> ('a -> 'b) -> 'b parser =
 fun p f s ->
  (* Format.(printf "fmap. input = '%a'\n" (pp_print_list pp_print_char) s); *)
  match p s with Failed -> Failed | Parsed (x, tl) -> return (f x) tl

let ( <*> ) : 'a 'b. ('a -> 'b) parser -> 'a parser -> 'b parser =
 fun f arg input ->
  match f input with
  | Failed -> Failed
  | Parsed (f, tl) -> (
      match arg tl with Failed -> Failed | Parsed (x, tl) -> Parsed (f x, tl))

(* *************************************************************** *)
let many : 'a parser -> 'a list parser =
 fun p ->
  let rec helper s =
    (* Format.(printf "input = '%a'\n" (pp_print_list pp_print_char) s); *)
    match p s with
    | Failed -> return [] s
    | Parsed (x, tl) -> (helper >>= fun tl -> return (List.cons x tl)) tl
    (* (helper >>| List.cons x) tl *)
    (* TODO: Think why  rewrite with >>| increases stack consumption? *)
  in
  helper

let%test _ =
  let p = many (char 'a') in
  let input = [ 'b'; 'a'; 'b' ] in
  Parsed ([], input) = p input

let%test _ =
  let p = many (char 'b') in
  p [ 'b'; 'b'; 'a' ] = Parsed ([ 'b'; 'b' ], [ 'a' ])

let many1 p : _ list parser =
  p >>= fun x ->
  many p >>= fun xs -> return (x :: xs)

let%test _ =
  let p = many1 (char 'b') in
  p [ 'b'; 'b'; 'a' ] = Parsed ([ 'b'; 'b' ], [ 'a' ])

let%test _ =
  let p = many1 (char 'a') in
  Failed = p [ 'b'; 'a'; 'b' ]

(* ************** Alternatives  ********************************** *)
let ( <|> ) : 'a parser -> 'a parser -> 'a parser =
 fun p1 p2 s -> match p1 s with Failed -> p2 s | ok -> ok

let a_or_b = char 'a' <|> char 'b'
let%test _ = a_or_b [ 'a' ] = return 'a' []
let%test _ = a_or_b [ 'b' ] = return 'b' []
let%test _ = a_or_b [ 'c' ] = Failed
let choice = function [] -> fail | h :: tl -> List.fold_left ( <|> ) h tl

module Syntax = struct
  (* Писать инфиксные операторы порой не удобно,
     можно завести специальный синтаксиc (аппликативной) do-нотации *)
  let ( let* ) = ( >>= )
  let ( let+ ) = ( >>| )

  let ( and+ ) f1 f2 input =
    match f1 input with
    | Failed -> Failed
    | Parsed (l, input) -> (
        match f2 input with
        | Failed -> Failed
        | Parsed (r, rest) -> Parsed ((l, r), rest))
end

open Syntax

module Arithmetic1 = struct
  type expr = Const of int | Plus of expr * expr [@@deriving show]

  (* <expr> -- это <digit> + <expr> или <digit>
  *)

  let parser =
    let const = digit >>= fun d -> return (Const d) in
    let rec p s =
      choice
        [
          (let* l = const in
           let* r = char '+' *> p in
           return (Plus (l, r)));
          const;
        ]
        s
    in
    p

  let%test _ =
    let input = [ '5'; '+'; '6'; '+'; '9' ] in
    let rez = parser input in
    (* Format.printf "%a\n%!" (pp_parse_result pp_expr) rez; *)
    rez = return (Plus (Const 5, Plus (Const 6, Const 9))) []

  (* let parser =
     let const = digit >>= fun d -> return (Const d) in
     let rec p s =
       (const
       >>= (fun l -> char '+' *> p >>= fun r -> return (Plus (l, r)))
       <|> const)
         s
     in
     p *)
end

(* ********** Parsing of arithmetic BAD ******************************** *)
module Arithmetic2 = struct
  type expr = Const of int | Plus of expr * expr

  (* <expr> -- это <expr> + <digit>  или <digit>
     Левая рекурсия стреляет в ногу
  *)
  let parser =
    let const = digit >>= fun d -> return (Const d) in
    let rec p s =
      ( p >>= fun l ->
        char '+' >>= fun _ ->
        const >>= fun r -> return (Plus (l, r)) <|> const )
        s
    in
    p

  let%test _ =
    try
      let (_ : bool) =
        parser [ '1'; '+'; '2' ] = return (Plus (Const 1, Const 2)) []
      in
      false
    with Stack_overflow -> true
end

(* ********** Parsing of arithmetic GOOD ******************************* *)
let ints_list : _ parser =
  digit >>= fun h ->
  many (satisfy (Char.equal '+') *> digit) >>= fun tl -> return (h :: tl)

let%test _ = ints_list [ '1'; '+'; '2'; '+'; '3' ] = return [ 1; 2; 3 ] []

let%test _ =
  ints_list [ '1'; '+'; '2'; '+'; '+'; '3' ] = return [ 1; 2 ] [ '+'; '+'; '3' ]

let ints_sum =
  let next = satisfy (Char.equal '+') *> digit in
  let rec helper acc s =
    match next s with
    | Failed -> return acc s
    | Parsed (n, tl) -> helper (acc + n) tl
  in
  digit >>= helper

let%test _ = ints_sum [ '1'; '+'; '2'; '+'; '9' ] = return 12 []

let%test _ =
  ints_sum [ '1'; '+'; '2'; '+'; '+'; '9' ] = return 3 [ '+'; '+'; '9' ]

(* ************ Parsing with parenthesis *********************************** *)

module Arithmetic3 = struct
  type expr = Const of int | Plus of expr * expr [@@deriving show]

  (* <expr> -- это
      либо (<expr>)
      либо <digit> и n>=0 раз + <expr>
      либо <digit>
  *)

  (* let choice xs = List.fold_left ( <|> ) (fun _ -> Failed) xs *)
  let parens p = char '(' *> p <* char ')'

  let parser =
    let const = digit >>= fun d -> return (Const d) in
    let rec p s =
      choice
        [
          parens p;
          (let* h = const in
           let* tl = many (char '+' *> p) in
           return (List.fold_left (fun acc r -> Plus (acc, r)) h tl));
        ]
        s
    in
    p

  let%test _ =
    let input = [ '('; '5'; '+'; '6'; '+'; '9'; ')' ] in
    let rez = parser input in
    (* Format.printf "%a\n%!" (pp_parse_result pp_expr) rez; *)
    rez = return (Plus (Const 5, Plus (Const 6, Const 9))) []

  let%test _ =
    let input = [ '('; '5'; '+'; '('; '6'; '+'; '9'; ')'; ')' ] in
    let rez = parser input in
    (* Format.printf "%a\n%!" (pp_parse_result pp_expr) rez; *)
    rez = return (Plus (Const 5, Plus (Const 6, Const 9))) []
end

let implode chars =
  let b = Buffer.create (List.length chars) in
  List.iter (Buffer.add_char b) chars;
  Buffer.contents b

module Arithmetic4 = struct
  type expr = Const of int | Plus of expr * expr | Asterisk of expr * expr
  [@@deriving show { with_path = false }]

  let multi_sum h tl = List.fold_left (fun acc r -> Plus (acc, r)) h tl
  let multi_prod h tl = List.fold_left (fun acc r -> Asterisk (acc, r)) h tl
  (* <expr> -- это неформально (!!)
      либо (<expr>)
      либо <expr> + <expr>
      либо <expr> * <expr>
      либо <digit>
  *)

  let choice xs = List.fold_left ( <|> ) (fun _ -> Failed) xs
  let parens p = char '(' *> p <* char ')'

  let parser =
    let const = digit >>= fun d -> return (Const d) in
    let rec product s =
      (let* h = parens sum <|> const in
       let* tl = many (char '*' *> (parens sum <|> const)) in
       return (multi_prod h tl))
        s
    and sum input =
      (let* h = product <|> parens sum in
       let* tl = many (char '+' >>= fun _ -> product) in
       return (multi_sum h tl))
        input
    in
    sum

  let wrap input =
    match parser input with
    | Failed -> print_endline "fail"
    | Parsed (v, tl) ->
        Format.printf "%a\n%!" pp_expr v;
        Format.printf "%a\n%!" Format.(pp_print_list pp_print_char) tl

  let%expect_test _ =
    wrap [ '('; '6'; ')' ];
    [%expect {| Const (6) |}]

  let%expect_test _ =
    wrap [ '5'; '+'; '6'; '*'; '9' ];
    [%expect {| Plus (Const (5), Asterisk (Const (6), Const (9))) |}]

  let%expect_test _ =
    wrap [ '5'; '*'; '6'; '+'; '9' ];
    [%expect {| Plus (Asterisk (Const (5), Const (6)), Const (9)) |}]

  let%expect_test _ =
    wrap [ '5'; '*'; '('; '6'; '+'; '9'; ')' ];
    [%expect {| Asterisk (Const (5), Plus (Const (6), Const (9))) |}]

  let%expect_test _ =
    wrap [ '('; '5'; '+'; '6'; ')'; '*'; '9' ];
    [%expect {| Asterisk (Plus (Const (5), Const (6)), Const (9)) |}]

  let%expect_test _ =
    wrap [ '1'; '+'; '('; '5'; '+'; '6'; ')'; '*'; '9' ];
    [%expect
      {| Plus (Const (1), Asterisk (Plus (Const (5), Const (6)), Const (9))) |}]
end

module Test (Parser : sig
  [@@@ocaml.warning "-unused-type-declaration"]

  type 'a parse_result
  type 'a t
  (* type 'a t = char list -> 'a parse_result *)

  val fix : ('a t -> 'a t) -> 'a t
  val char : char -> char t
  val digit : int t
  val ( <|> ) : 'a t -> 'a t -> 'a t
  val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t
  val return : 'a -> 'a t
  val many : 'a t -> 'a list t
  (* val fail : string -> 'a t *)

  val run : 'a t -> char list -> 'a option

  (* val to_result : 'a parse_result -> ('a * char list, unit) Result.t *)
end) =
struct
  open Parser

  type expr = Const of int | Plus of expr * expr | Asterisk of expr * expr
  [@@deriving show { with_path = false }]

  (* <expr> -- это неформально (!!)
      либо (<expr>)
      либо <expr> + <expr>
      либо <expr> * <expr>
      либо <digit>
  *)

  let choice2 = ( <|> )

  let parens p =
    char '(' >>= fun _ ->
    p >>= fun x ->
    char ')' >>= fun _ -> return x

  type dispatch = { prod : dispatch -> expr t; sum : dispatch -> expr t }

  let const = digit >>= fun d -> return (Const d)

  (* a^n b^n c^n  *)

  (*
     IDENT * IDENT
    *)

  let many2 p =
    fix (fun many ->
        p >>= fun h ->
        many >>= fun tl -> return (List.cons h tl))

  let product ({ sum; _ } as d) =
    fix (fun product ->
        choice2
          (parens (sum d))
          ( const >>= fun h ->
            many (char '*' >>= fun _ -> product) >>= fun tl ->
            return (List.fold_left (fun acc r -> Asterisk (acc, r)) h tl) ))

  let sum ({ prod = product; sum } as d) =
    fix (fun _sum ->
        choice2
          (parens (sum d))
          ( product d >>= fun h ->
            many (char '+' >>= fun _ -> product d) >>= fun tl ->
            return (List.fold_left (fun acc r -> Plus (acc, r)) h tl) ))

  let d : dispatch = { sum; prod = product }
  let arith : expr t = d.sum d

  let%test _ =
    let input = [ '5'; '+'; '6'; '*'; '9' ] in
    match run arith input with
    | Some r ->
        (* Format.printf "%a\n%!" pp_expr r; *)
        r = Plus (Const 5, Asterisk (Const 6, Const 9))
    | _ -> false

  let%test _ =
    let _ = assert ("asdf\"qwe" = {|asdf"qwe|}) in

    let input = [ '5'; '*'; '6'; '+'; '9' ] in
    match run arith input with
    | Some r ->
        (* Format.printf "%a\n%!" pp_expr r; *)
        r = Plus (Asterisk (Const 5, Const 6), Const 9)
    | _ -> false
end

module M = Test (struct
  type 'a parse_result
  type 'a t = 'a parser

  let ( >>= ) = ( >>= )

  (* let choice2 a b = choice [ a; b ] *)
  let many = many
  let ( <|> ) = ( <|> )
  let return = return
  let char = char
  let digit = digit
  let run p s = match p s with Failed -> None | Parsed (x, _) -> Some x

  let fix : ('a t -> 'a t) -> 'a t =
    let rec fix f s = f (fix f) s in
    fix
end)
(* ********************************** The End ********************************* *)

(* Бывают разные варианты как объявить input
   - char list
   - int * string
   - int * char Seq.t
   - с поддержкой юникода
   - графы

   Результат парсинга тоже бывает разный. Например, можно добавить конструктор NeedMoreInput.

   Или можно возвращать пустой список, если не удалось распарсить, или несколько ответов, если
   можно распарсить более чем 1м способом

     type 'a parse_result = ('a * input) list
*)

let rec take_while : (char -> bool) -> unit parser =
 fun f input ->
  match input with
  | h :: tl when f h -> take_while f tl
  | _ -> Parsed ((), input)

let ws = take_while (function ' ' | '\t' | '\n' | '\r' -> true | _ -> false)

let parse_string p str =
  let input = List.init (String.length str) (fun n -> str.[n]) in
  match p input with
  | Failed -> Result.error ""
  | Parsed (_, _ :: _) -> Result.error ": end_of_input "
  | Parsed (x, []) -> Result.ok x

let test parser pp input =
  let rez = parse_string (parser <* ws) input in
  match rez with
  | Result.Ok v -> Format.printf "%a\n%!" pp v
  | Result.Error e -> Format.printf "Error: '%s'\n" e
(**)
