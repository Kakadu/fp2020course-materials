(* Don't show tomorrow *)

open Benchmark
open Lib

let () =
  let res =
    throughputN
      ~repeat:5
      1
      [ "recursive fib_rec", fib_rec, 10
      ; "cps ", fib_cps, 10
      ; "cps monad", fib_cps_m, 10
      ]
  in
  tabulate res
;;
