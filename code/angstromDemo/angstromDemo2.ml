(** Copyright 2021-2023, Kakadu *)

(** SPDX-License-Identifier: LGPL-3.0-or-later *)

open Angstrom

let number = char '0' >>| String.make 1
let eadd a b = Printf.sprintf "(%s+%s)" a b
let esub a b = Printf.sprintf "(%s-%s)" a b
let emul a b = Printf.sprintf "(%s*%s)" a b
let ediv a b = Printf.sprintf "(%s/%s)" a b
let eeq a b = Printf.sprintf "(%s=%s)" a b
let elt a b = Printf.sprintf "(%s<%s)" a b

let prio expr table =
  let len = Array.length table in
  let rec helper level =
    if level >= len then expr
    else
      let xs = table.(level) in
      return (List.fold_left (fun acc (op, r) -> op acc r))
      <*> helper (level + 1)
      <*> many
            (choice
               (List.map
                  (fun (op, f) ->
                    op *> helper (level + 1) >>= fun r -> return (f, r))
                  xs))
  in
  helper 0

let demo_prio =
  prio number
    [|
      [ (string "<", elt); (string "=", eeq) ];
      [ (string "+", eadd); (string "-", esub) ];
      [ (string "*", emul); (string "/", ediv) ];
    |]

let () =
  let pp_result ppf = function
    | Ok x | Error x -> Format.pp_print_string ppf x
  in
  Format.printf "%a\n%!" pp_result
  @@ parse_string ~consume:Consume.All demo_prio "0*0+0*0<0"

let%test _ =
  parse_string ~consume:Consume.All demo_prio "0*0+0*0" = Ok "((0*0)+(0*0))"

let%test _ = parse_string ~consume:Consume.All demo_prio "0-0" = Ok "(0-0)"

let%test _ =
  parse_string ~consume:Consume.All demo_prio "0*0-0" = Ok "((0*0)-0)"

let%test _ =
  parse_string ~consume:Consume.All demo_prio "0*0<0+0/0"
  = Ok "((0*0)<(0+(0/0)))"
