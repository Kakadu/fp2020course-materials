let rec ones = 1 :: ones

module Parser : sig end = struct
  open Angstrom

  [@@@warning "-32"]

  let in_parens p = char '(' *> p <* char ')'
  (* let rec pars = many (in_parens pars) *> return () *)
end

module Parser2 : sig end = struct
  type 'a t = string -> ('a * string) option

  [@@@warning "-32"]

  let char : char -> char t = fun _ -> assert false
  let ( *> ) : 'a t -> 'b t -> 'b t = fun _ _ -> assert false
  let ( <* ) : 'a t -> 'b t -> 'a t = fun _ _ -> assert false
  let return : 'a -> 'a t = fun _ -> assert false
  let many : 'a t -> 'a list t = fun _ -> assert false
  let in_parens p = char '(' *> p <* char ')'
  let rec pars eta = (many (in_parens pars) *> return ()) eta
end

module Restriction = struct
  [@@@warning "-32"]

  (* let foo : 'a. 'a -> 'a =
       let _aaa = ref None in
       fun x -> x

     let foo : 'a. 'a -> 'a =
       let save = ref None in
       fun x ->
         save := Some x;
         x

     let foo : 'a. 'a -> 'a =
       let save = ref None in
       fun x ->
         save := Some x;
         x *)
end

let _ =
  let id x = x in
  (id 1, id "")

(* let _ = (fun id -> (id 1, id "")) (fun x -> x) *)
