open Expr02
open Printf

let timeout = 2.0
let program = Plus (Var "x", Var "x")

let () =
  print_endline "Slow mode";
  let module E = Eval (SlowId) in
  E.eval
    (function
      | "x" ->
          print_endline "Sleeping";
          let () = Thread.delay timeout in
          Format.printf "Got 2\n%!";
          2
      | _ -> assert false)
    program
  |> printf "%d\n%!"

let () =
  print_endline "Concurrent mode";
  let open Lwt in
  let open Lwt.Syntax in
  let module E = Eval (ConcurrentId) in
  E.eval
    (function
      | "x" ->
          print_endline "Sleeping";
          let* () = Lwt_unix.sleep timeout in
          return 2
      | _ -> assert false)
    program
  |> Lwt_main.run |> printf "%d\n%!"
