open Base
open Ast
open Angstrom

let log fmt =
  Format.kfprintf (fun ppf -> Format.fprintf ppf "\n%!") Format.std_formatter fmt
;;

let pp_list f ppf xs =
  Format.pp_print_list ~pp_sep:(fun ppf () -> Format.fprintf ppf " ") f ppf xs
;;

let ws =
  skip_while (function
      | '\x20' | '\x0a' | '\x0d' | '\x09' -> true
      | _ -> false)
;;

let trace_pos msg =
  pos
  >>= fun n ->
  log "`%s` on pos %d" msg n;
  return ()
;;

let trace_avai msg =
  available
  >>= fun n ->
  log "`%s` there are %d available." msg n;
  return ()
;;

let lchar c = ws *> char c

let parens p =
  char '('
  *> (* trace_pos "after(" *> *)
  p
  <* (* trace_pos "before ')'" <*  *)
  lchar ')'
;;

(* <* trace_pos "after parens" *)

let const = char '0' >>= fun c -> return (Printf.sprintf "%c" c)

type dispatch =
  { expr : dispatch -> named t
  ; expr_long : dispatch -> named t
  }

let is_digit = function
  | '0' .. '9' -> true
  | _ -> false
;;

let to_digit c = Char.to_int c - Char.to_int '0'

let digit =
  any_char
  >>= function
  | '0' .. '9' as c -> return (Char.to_int c - Char.to_int '0')
  | _ -> fail ""
;;

let number =
  (* trace_pos "number" *>  *)
  digit
  >>= fun h ->
  scan_state h (fun st c -> if is_digit c then Some ((10 * st) + to_digit c) else None)
;;

(* >>| fun r -> log "number %d parssed" r; r *)

let is_alpha = function
  | 'a' .. 'z' | 'A' .. 'Z' -> true
  | _ -> false
;;

let alpha_c =
  any_char
  >>= function
  | c -> if is_alpha c then return c else fail (Format.sprintf "'%c' not a letter" c)
;;

let is_keyword = function
  | "fun" -> true
  | _ -> false
;;

let ident =
  alpha_c
  >>= fun h ->
  many alpha_c
  >>= fun tl ->
  let name = String.of_char_list (h :: tl) in
  if is_keyword name then fail "a keyword" else return name
;;

(* >>= fun h ->
  take_while (fun c -> is_alpha c || is_digit c)
  >>= fun tl ->
  let id = Format.sprintf "%c%s" h tl in
  if is_keyword id
  then fail "ident looks like a keyword"
  else (* let () = log "ident '%s' parsed" id in *)
    ws *> return id *)

let string s =
  (* trace_pos (Format.sprintf "string `%s`" s) *>  *)
  string s
;;

let pack : dispatch =
  let expr d =
    fix (fun _self ->
        ws
        *> (parens (d.expr_long d)
           <|> (parens (d.expr d) <|> (ws *> ident >>| fun s -> Var s))
           <|> ((string "fun" <|> string "λ") *> ws *> ident
               >>= fun p ->
               ws *> (string "->" <|> string ".") *> ws *> d.expr_long d
               >>= fun b -> return (Abs (p, b)))))
  in
  let expr_long d =
    fix
    @@ fun _self ->
    d.expr d
    >>= fun h ->
    many (d.expr d)
    >>= fun xs -> return @@ List.fold_left ~init:h xs ~f:(fun l r -> App (l, r))
  in
  { expr; expr_long }
;;

let parse p str = parse_string ~consume:All (p pack) str
