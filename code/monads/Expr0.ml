(** Copyright 2021-2023, Kakadu *)

(** SPDX-License-Identifier: LGPL-3.0-or-later *)

(* Do not show on Friday *)

(* Base case. Not very interesting *)

type expr =
  | Const of int
  | Plus of expr * expr
  | Asterisk of expr * expr

let eval =
  let rec helper = function
    | Const n -> n
    | Plus (l, r) -> helper l + helper r
    | Asterisk (l, r) -> helper l * helper r
  in
  helper

let%test _ =
  7 = eval (Plus (Const 1, Asterisk (Const 2, Const 3)))

(*
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *)
module MultipleAnswers = struct
  type expr =
    | Const of int
    | Plus of expr * expr
    | Asterisk of expr * expr

  let eval =
    let rec helper = function
      | Const n -> [ -n; n ]
      | Plus (l, r) ->
          let ls = helper l in
          let rs = helper r in
          List.concat
            (List.map (fun l -> List.map (( + ) l) rs) ls)
      | Asterisk (l, r) ->
          List.concat_map
            (fun l ->
              List.concat_map
                (fun r -> [ l * r ])
                (helper r))
            (helper l)
    in
    helper

  let%test _ =
    [ -1; 1 ] = List.sort compare @@ eval (Const 1)

  let%test _ =
    [ -2; 0; 0; 2 ]
    = List.sort compare @@ eval (Plus (Const 1, Const 1))

  let%test _ =
    [ -3; -1; 1; 3 ]
    = List.sort compare @@ eval (Plus (Const 1, Const 2))
end

(*
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *)
module MultipleAnswersWithApplicative = struct
  let pure x = [ x ]
  let ( <|> ) = List.append (* TODO: rename to something *)

  let ( <*> ) : 'a 'b. ('a -> 'b) list -> 'a list -> 'b list
      =
   fun fs xs -> List.concat_map (fun f -> List.map f xs) fs

  type expr =
    | Const of int
    | Plus of expr * expr
    | Asterisk of expr * expr

  let eval =
    let rec helper = function
      | Const n -> pure (-n) <|> pure n
      | Plus (l, r) -> pure ( + ) <*> helper l <*> helper r
      | Asterisk (l, r) ->
          pure ( * ) <*> helper l <*> helper r
    in
    helper

  let%test _ =
    [ -1; 1 ] = List.sort compare @@ eval (Const 1)

  let%test _ =
    [ -2; 0; 0; 2 ]
    = List.sort compare @@ eval (Plus (Const 1, Const 1))

  let%test _ =
    [ -3; -1; 1; 3 ]
    = List.sort compare @@ eval (Plus (Const 1, Const 2))
end
(*
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *)

module type FUNCTOR = sig
  type 'a t

  val fmap : 'a t -> ('a -> 'b) -> 'b t

  module Syntax : sig
    val ( let+ ) : 'a t -> ('a -> 'b) -> 'b t
  end
end

module type ALTERNATIVE = sig
  type 'a t

  val ( <|> ) : 'a t -> 'a t -> 'a t
end

module type APPLICATIVE = sig
  type 'a t

  val pure : 'a -> 'a t
  (** Same as return *)

  val ( <*> ) : ('a -> 'b) t -> 'a t -> 'b t

  (*
      val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t

      *)
  val ( >>| ) : 'a t -> ('a -> 'b) -> 'b t

  module Syntax : sig
    val ( let+ ) : 'a t -> ('a -> 'b) -> 'b t
    (* val ( and+ ) : 'a t -> 'b t -> ('a * 'b) t *)
  end
end

module type ARG = sig
  include APPLICATIVE
  include ALTERNATIVE with type 'a t := 'a t
end

module Eval (M : ARG) = struct
  open M

  type expr =
    | Const of int
    | Plus of expr * expr
    | Asterisk of expr * expr

  let eval =
    let rec helper = function
      | Const n -> pure n <|> pure (-n)
      | Plus (l, r) -> pure ( + ) <*> helper l <*> helper r
      | Asterisk (l, r) ->
          pure ( + ) <*> helper l <*> helper r
    in
    helper
end

module _ = struct
  module L = struct
    type 'a t = 'a list

    let ( <|> ) = ( @ )
    let pure x = [ x ]
    let fmap = List.map
    let ( >>| ) xs f = fmap f xs

    let ( <*> ) :
          'a 'b. ('a -> 'b) list -> 'a list -> 'b list =
     fun fs xs ->
      List.concat_map (fun f -> List.map f xs) fs

    module Syntax = struct
      let ( let+ ) = ( >>| )
    end
  end

  module E = Eval (L)
  open E

  let%test _ =
    [ -1; 1 ] = List.sort compare @@ eval (Const 1)

  let%test _ =
    [ -2; 0; 0; 2 ]
    = List.sort compare @@ eval (Plus (Const 1, Const 1))

  let%test _ =
    [ -3; -1; 1; 3 ]
    = List.sort compare @@ eval (Plus (Const 1, Const 2))
end

module _ = struct
  module Option = struct
    type 'a t = 'a option

    let ( <|> ) a b = match a with None -> b | _ -> a
    let pure x = Some x
    let fmap = Option.map
    let ( >>| ) xs f = fmap f xs

    let ( <*> ) : 'a 'b. ('a -> 'b) t -> 'a t -> 'b t =
     fun fs xs ->
      match (fs, xs) with
      | Some f, Some x -> Some (f x)
      | _ -> None

    module Syntax = struct
      let ( let+ ) = ( >>| )
    end
  end

  module E = Eval (Option)
  open E

  let%test _ = Some 1 = eval (Const 1)
  let%test _ = Some 2 = eval (Plus (Const 1, Const 1))
  let%test _ = Some 3 = eval (Plus (Const 1, Const 2))
end

module Id = struct
  type 'a t = 'a

  let ( <|> ) a _ = a
  let pure x = x
  let fmap f x = f x
  let ( >>| ) xs f = fmap f xs

  let ( <*> ) : 'a 'b. ('a -> 'b) t -> 'a t -> 'b t =
   fun fs xs -> fs xs

  module Syntax = struct
    let ( let+ ) = ( >>| )
  end
end

module _ = struct
  module E = Eval (Id)
  open E

  let%test _ = 1 = eval (Const 1)
  let%test _ = 2 = eval (Plus (Const 1, Const 1))
  let%test _ = 3 = eval (Plus (Const 1, Const 2))
end
