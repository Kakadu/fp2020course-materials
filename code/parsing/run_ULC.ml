[@@@ocaml.text "/*"]

(** Copyright 2021-2023, Kakadu *)

(** SPDX-License-Identifier: LGPL-3.0-or-later *)

[@@@ocaml.text "/*"]

open ULC

let __ () =
  Format.printf "%a\n%!" Lam.pp
    (Result.get_ok
    @@ Angstrom.parse_string
         (parse_lam.single parse_lam)
         "(z t)" ~consume:Angstrom.Consume.Prefix)

let generate n =
  List.iter
    Format.(fprintf std_formatter "%a\n" PP.pp)
    (QCheck.Gen.generate ~n ULC.TestQCheck.lam_gen_manual)

let () =
  Arg.parse
    [
      ("-seed", Arg.Int QCheck_runner.set_seed, " Set seed");
      ("-stop", Arg.Unit (fun _ -> exit 0), " Exit");
      ("-gen", Arg.Int generate, " Exit");
    ]
    (fun _ -> assert false)
    "help"

let () =
  print_endline "Testing manual generator.";
  let _ : int = ULC.TestQCheck.run_manual () in
  ()

let () =
  print_endline "Testing derived generator.";
  let _ : int = ULC.TestQCheck.run_auto () in
  ()
