(** Схемы рекурсии для списков: ката-, ана- пара- и хиломорфизмы*)

(**
В идеале это надо делать для ленивых последовательностей,

  type 'a stream = Nil | Cons of 'a * 'a stream Lazy.t

но для простоты тут станадртные списки
*)

module Cata = struct
  (* cata === "вовнутрь", "вниз" *)

  let cata_ = List.fold_right
  let rec cata f e xs = match xs with [] -> e | x :: xs -> f x (cata f e xs)
  let%test _ = 24 = cata ( * ) 1 [ 4; 3; 2; 1 ]

  (* Сортировка вставками *)
  let isort xs =
    let rec insert x lst =
      match lst with
      | [] -> [ x ]
      | h :: xs when x < h -> x :: h :: xs
      | h :: xs -> h :: insert x xs
    in
    cata_ insert xs []

  let%test _ = isort [ 1; 2; 3; 4 ] = [ 1; 2; 3; 4 ]
  let%test _ = isort [ 2; 1; 4; 3 ] = [ 1; 2; 3; 4 ]
end

module Ana = struct
  (* ***** Unfold -- дуальная функция к fold  *********** *)
  (* Анаморфизм -- "преобразование наружу" (греч.) *)
  let rec ana1 : 'a 'b. ('a -> ('b * 'a Lazy.t) option) -> 'a -> 'b list =
   fun f u ->
    match f u with None -> [] | Some (x, v) -> x :: ana1 f (Lazy.force v)

  let rec ana : 'a 'b. ('a -> bool) -> ('a -> 'b) -> ('a -> 'a) -> 'a -> 'b list
      =
   fun fin get next b ->
    if fin b then [] else get b :: ana fin get next (next b)

  let%test _ = [ 4; 3; 2; 1 ] = ana (fun x -> x <= 0) Fun.id (fun x -> x - 1) 4

  module SelectionSort = struct
    open Cata

    let minimum = function
      | x :: xs -> cata min x xs
      | _ -> failwith "bad argument"

    let rec delete y = function
      | [] -> []
      | x :: xs when y = x -> xs
      | x :: xs -> x :: delete y xs

    let delmin = function
      | [] -> None
      | xs ->
          let y = minimum xs in
          Some (y, lazy (delete y xs))

    let sort xs = ana1 delmin xs
    let%test _ = sort [ 4; 3; 2; 1 ] = [ 1; 2; 3; 4 ]
  end

  (* ******* Сортировка пузырьком через unfold ******** *)
  module BubbleSort = struct
    open Cata

    (* С каждым шагом [x] всплывает *)
    let step x = function
      | None -> Some (x, lazy [])
      | Some (y, ys) when x < y -> Some (x, lazy (y :: Lazy.force ys))
      | Some (y, ys) -> Some (y, lazy (x :: Lazy.force ys))

    let bubble xs = cata step None xs
    let sort xs = ana1 bubble xs
    let%test _ = sort [ 4; 3; 2; 1 ] = [ 1; 2; 3; 4 ]

    let step_strict x = function
      | None -> Some (x, [])
      | Some (y, ys) when x < y -> Some (x, y :: ys)
      | Some (y, ys) -> Some (y, x :: ys)

    let bubble xs = cata step_strict None xs
    let%test _ = bubble [ 4; 3; 2; 1 ] = Some (1, [ 4; 3; 2 ])
    let%test _ = bubble [ 4; 1; 2; 3 ] = Some (1, [ 4; 2; 3 ])
  end
end

(**  Упражнение
  * Если описать стрим как тип
  *    type 'a stream = Nil | Cons of 'a * 'a stream Lazy.t
  *  то функция "дайте n минимальных элементов в стриме" пишется как
  *    let take_n_smallest xs = take n (sort xs)
  *)

module Para = struct
  open Cata

  module BuggyPara : sig
    val para : ('a -> 'a list * 'b -> 'b) -> 'b -> 'a list -> 'b
    val isort : ('a -> 'a -> bool) -> 'a list -> 'a list
  end = struct
    let rec para f e xs =
      match xs with [] -> e | x :: xs -> f x (xs, para f e xs)

    let insert lt x lst =
      para
        (fun h -> function
          | tl, _ when lt x h -> x :: h :: tl
          | _, acc -> h :: acc)
        [ x ] lst

    let isort lt xs = cata_ (insert lt) xs []
    let%test _ = isort ( < ) [ 1; 2; 3; 4 ] = [ 1; 2; 3; 4 ]
    let%test _ = isort ( < ) [ 4; 3; 2; 1 ] = [ 1; 2; 3; 4 ]
    let%test _ = insert ( < ) 2 [ 1; 3 ] = [ 1; 2; 3 ]
  end

  module GoodPara = struct
    let rec para f e xs =
      match xs with [] -> e | x :: xs -> f x (xs, lazy (para f e xs))

    let isort lt xs =
      let insert x lst =
        para
          (fun h -> function
            | tl, _ when lt x h -> x :: h :: tl
            | _, acc -> h :: Lazy.force acc)
          [ x ] lst
      in
      List.fold_right insert xs []

    let%test _ = isort ( < ) [ 1; 2; 3; 4 ] = [ 1; 2; 3; 4 ]
    let%test _ = isort ( < ) [ 4; 3; 2; 1 ] = [ 1; 2; 3; 4 ]
  end

  module CheckCounter = struct
    let () =
      let counter = ref 0 in
      let lt a b =
        incr counter;
        a < b
      in
      assert (BuggyPara.isort lt [ 1; 2; 3; 4 ] = [ 1; 2; 3; 4 ]);
      assert (6 = !counter)

    let () =
      let counter = ref 0 in
      let lt a b =
        incr counter;
        a < b
      in
      assert (GoodPara.isort lt [ 1; 2; 3; 4 ] = [ 1; 2; 3; 4 ]);
      assert (3 = !counter)
  end
end

module Hylo = struct
  open Ana
  open Cata

  (* Hylomophimsm: fold after unfold *)
  let fact n =
    assert (n >= 0);
    cata ( * ) 1 (ana (( = ) 0) Fun.id pred n)

  let%test _ = ana (( = ) 0) Fun.id pred 5 = [ 5; 4; 3; 2; 1 ]
  let%test _ = fact 5 = 120
  let hylo f e p g h eta = cata f e (ana p g h eta)
  let fact = hylo ( * ) 1 (( = ) 0) Fun.id pred
  let%test _ = fact 5 = 120

  (* N.B. deforestated implementation *)
  let rec hylo f e p g h eta =
    if p eta then e else f (g eta) (hylo f e p g h (h eta))

  (* Proof by Term Rewriting *)

  (*
  let hylo f e p g h eta = cata f e (ana p g h eta)
  let hylo f e p g h eta =
    if p eta
    then cata f e []
    else
      cata f e (g eta :: ana p g h (h eta))

  let hylo f e p g h eta =
    if p eta
    then e
    else
      f (g eta) (cata f e (ana p g h (h eta)))

  let rec hylo f e p g h eta =
    if p eta
    then e
    else
      f (g eta) (hylo f e p g h (h eta))
  *)

  let%test _ =
    let fact = hylo ( * ) 1 (( = ) 0) Fun.id pred in
    fact 5 = 120

  (*
  Упражение: Напишите функцию char list -> bool list, которая конвертирует
  десятичное представление числа в бинарное, с помощью unfold после fold

*)

  let rec map f = function [] -> [] | x :: xs -> f x :: map f xs
  let _ = map string_of_int (map (( + ) 1) [ 1; 2; 3; 4; 5 ])

  (*
   map f (map g xs) === map (fun x -> f (g x)) xs


  map f (map g xs)

  = function
  | [] -> map f []
  | x :: xs -> map f (g x :: map g xs)


    = function
  | [] -> []
  | x :: xs -> (f (g x)) :: (map f (map g xs))


  h x === f (g x)

  map h = function
  | [] -> []
  | x :: xs -> (h x) :: map h xs

*)
end

(* TODO: apomorphisms не очень интересные *)
