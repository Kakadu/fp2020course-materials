(* https://kcsrk.info/cs3100_f19/lectures/lec14/lec14.pdf *)

(** Normal lists *)
let rec ones = 1 :: ones

let rec zero_ones = 0 :: 1 :: zero_ones

let%test _ =
  try
    let _zero_ones_string = List.map string_of_int zero_ones in
    false
  with Stack_overflow -> true

(* Обычный список *)
type 'a list = 'a Stdlib.List.t = [] | ( :: ) of 'a * 'a list

module Bad = struct
  (* Вот такое не сработает *)
  type 'a stream = Cons of 'a * 'a stream
end

(* Приостановка исполнения *)
let f () = failwith "error"

let%test _ =
  try
    let _ = f () in
    false
  with Failure _ -> true

(* А вот это правильно *)
(* Так сделано в модуле Seq *)
type 'a seq = Cons of 'a * (unit -> 'a seq) | Nil

let rec zero_ones = Cons (0, fun () -> Cons (1, fun () -> zero_ones))

(* Don't use it in production code base, if you are not sure *)
let hd_exn = function Nil -> failwith "bad argument" | Cons (x, _) -> x

(* Don't use it in production code base, if you are not sure *)
let tl_exn = function Nil -> failwith "bad argument" | Cons (_, xs) -> xs ()
let rec take n s = if n = 0 then [] else hd_exn s :: take (n - 1) (tl_exn s)
let%test _ = take 10 zero_ones = [ 0; 1; 0; 1; 0; 1; 0; 1; 0; 1 ]
let rec drop n s = if n = 0 then s else drop (n - 1) (tl_exn s)

let rec map f s =
  let tl = tl_exn s in
  Cons (f (hd_exn s), fun () -> map f tl)

let zero_ones_str = map string_of_int zero_ones

let%test _ =
  take 10 zero_ones_str = [ "0"; "1"; "0"; "1"; "0"; "1"; "0"; "1"; "0"; "1" ]

let rec filter p s =
  if p (hd_exn s) then filter p (tl_exn s)
  else Cons (hd_exn s, fun () -> filter p (tl_exn s))

let%test _ =
  let s' = filter (( = ) 0) zero_ones in
  take 10 s' = [ 1; 1; 1; 1; 1; 1; 1; 1; 1; 1 ]

let rec zip f s1 s2 =
  Cons (f (hd_exn s1) (hd_exn s2), fun () -> zip f (tl_exn s1) (tl_exn s2))

(**
  * EXERCISE:  Fibonacci
  *)
let rec fib = Cons (0, fun () -> Cons (1, fun () -> zip ( + ) fib (tl_exn fib)))

let%test _ = take 11 fib = [ 0; 1; 1; 2; 3; 5; 8; 13; 21; 34; 55 ]

(**
  *  Erathosphene's prime numbers?
  *  EXERCISE
  *)

(* *************** Ещё более ленивые списки ************** *)

(* Проблема: Оно считает повторно одно и то же *)

let () =
  let c = ref 0 in
  let xs : unit seq = map (fun _ -> incr c) zero_ones in
  let (_ : unit list) = take 10 xs in
  assert (!c == 11);
  let (_ : unit list) = take 10 xs in
  assert (!c == 21)

(** https://courses.cs.cornell.edu/cs3110/2021sp/textbook/adv/lazy.html *)
module LazyFibs = struct
  type 'a lazystream = Cons of 'a * 'a lazystream Lazy.t

  let hd : 'a lazystream -> 'a = fun (Cons (h, _)) -> h
  let tl : 'a lazystream -> 'a lazystream = fun (Cons (_, t)) -> Lazy.force t

  let rec take_aux n (Cons (h, t)) lst =
    if n = 0 then lst else take_aux (n - 1) (Lazy.force t) (h :: lst)

  let take : int -> 'a lazystream -> 'a list =
   fun n s -> List.rev (take_aux n s [])

  let nth : int -> 'a lazystream -> 'a =
   fun n s -> List.hd (take_aux (n + 1) s [])

  let rec sum : int lazystream -> int lazystream -> int lazystream =
   fun (Cons (h_a, t_a)) (Cons (h_b, t_b)) ->
    Cons (h_a + h_b, lazy (sum (Lazy.force t_a) (Lazy.force t_b)))

  let rec map f xs =
    match xs with Cons (x, tl) -> Cons (f x, lazy (map f (Lazy.force tl)))

  let rec fibs = Cons (0, lazy (Cons (1, lazy (sum (tl fibs) fibs))))
  let nth_fib n = nth n fibs
  let rec zero_ones = Cons (0, lazy (Cons (1, lazy zero_ones)))

  let () =
    let c = ref 0 in
    let xs : unit lazystream = map (fun _ -> incr c) zero_ones in
    let (_ : unit list) = take 10 xs in
    assert (!c == 11);
    let (_ : unit list) = take 10 xs in
    assert (!c == 11)
end

let%test _ =
  LazyFibs.take 11 LazyFibs.fibs = [ 0; 1; 1; 2; 3; 5; 8; 13; 21; 34; 55 ]

(*
     type 'a stream = Nil | Cons of 'a * 'a stream Lazy.t

let rec zero_ones = lazy (Cons (0, lazy (Cons (1, zero_ones))))

let rec map f = function
  | (lazy Nil) -> lazy Nil
  | (lazy (Cons (x, tl))) -> lazy (Cons (f x, map f tl))

let hd_exn = function
  | (lazy Nil) -> failwith "bad argument"
  | (lazy (Cons (x, _))) -> x

let tl_exn = function
  | (lazy Nil) -> failwith "bad argument"
  | (lazy (Cons (_, tl))) -> tl

let rec take n s = if n = 0 then [] else hd_exn s :: take (n - 1) (tl_exn s)

let rec zipWith f xs ys =
  match (xs, ys) with
  | _, (lazy Nil) | (lazy Nil), _ -> lazy Nil
  | (lazy (Cons (x, tlx))), (lazy (Cons (y, tly))) ->
      lazy (Cons (f x y, zipWith f tlx tly))

      *)
