(** Copyright 2021-2023, Kakadu *)

(** SPDX-License-Identifier: LGPL-3.0-or-later *)

(* Errors via exception *)

type expr =
  | Const of int
  | Plus of expr * expr
  | Slash of expr * expr
  | Asterisk of expr * expr
  | Var of string
[@@deriving show]

let eval from_env =
  let ( let* ) x f = f x in
  let return x = x in

  let rec helper = function
    | Const n -> return n
    | Plus (l, r) ->
        let* l = helper l in
        let* r = helper r in
        return (l + r)
    | Asterisk (l, r) ->
        let* l = helper l in
        let* r = helper r in
        return (l * r)
    | Slash (l, r) ->
        let* r = helper r in
        if r = 0 then raise Division_by_zero
        else
          let* l = helper l in
          return (l / r)
    | Var s -> from_env s
  in
  helper

let%expect_test _ =
  print_int
  @@ eval
       (fun _ -> assert false)
       (Plus (Const 1, Asterisk (Const 2, Const 3)));
  [%expect {| 7 |}]

let%expect_test _ =
  (try
     eval (fun _ -> failwith "not found") (Var "x")
     |> print_int
   with
  | Failure s -> print_endline s
  | _ -> ());
  [%expect {| not found |}]
