(** Implementation with closures here *)

open Z3

module type S = sig
  type t
  type term

  val var : string -> term
  val ( =/= ) : term -> term -> t
end

let make_impl ctx =
  let module Impl = struct
    type t = Z3.Expr.expr
    type term = Z3.Expr.expr

    let var name =
      let sort1 =
        Enumeration.mk_sort ctx
          (Symbol.mk_string ctx "sort1")
          (List.map (Symbol.mk_int ctx) [1; 2]) in
      Expr.mk_fresh_const ctx name sort1

    let ( =/= ) a b = Boolean.mk_not ctx (Boolean.mk_eq ctx a b)
  end in
  (module Impl : S with type t = Z3.Expr.expr and type term = Z3.Expr.expr)

let () =
  let ctx = Z3.mk_context [] in
  let solver = Z3.Solver.mk_simple_solver ctx in
  let (module Impl) = make_impl ctx in
  let open Impl in
  let ca = var "a" in
  let cb = var "b" in
  let cc = var "c" in
  let check_and_report phs =
    match Solver.check solver phs with
    | Solver.UNKNOWN -> print_endline "unknown"
    | Solver.UNSATISFIABLE -> print_endline "UNSAT"
    | Solver.SATISFIABLE -> (
      match Solver.get_model solver with
      | None -> print_endline "no model"
      | Some m ->
          let get_var ca =
            Model.eval m ca false |> Option.get |> Expr.to_string in
          Format.printf "a = %s, b = %s, c = %s\n%!" (get_var ca) (get_var cc)
            (get_var cc) ) in
  let () = check_and_report [ca =/= cb; ca =/= cc] in
  let () = check_and_report [cb =/= cc] in
  let () = check_and_report [cb =/= cc; ca =/= cb; ca =/= cc] in
  ()
