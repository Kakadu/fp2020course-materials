Definition func_is_commutative {A B: Type} (f: A -> A -> B) : Prop :=
  forall (a b: A),  f a b = f b a.

Definition func_is_associative {A: Type} (f: A -> A -> A) :=
  forall (a b c: A),
      f a (f b c) = f (f a b) c.

Lemma test1 {A: Type} (f: A -> A -> A):
  func_is_associative f ->
  func_is_commutative f -> forall (x y z: A) ,
    f x (f y z) = f z (f y x).
Proof.
  intros.
  unfold func_is_commutative in H0.
  unfold func_is_associative in H.
  rewrite H; rewrite H.
  replace (f x y) with (f y x).
    2: { rewrite (H0 x y). }
  replace (f z y) with (f y z).
    2: { rewrite (H0 y z). }
  rewrite <- H. rewrite <- H.
  replace (f x z) with (f z x). 2: { rewrite (H0 x z). }
  reflexivity.
Qed.
