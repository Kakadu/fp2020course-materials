open Z3

let () =
  let ctx = Z3.mk_context [] in
  let solver = Z3.Solver.mk_simple_solver ctx in
  let make_sort name ints =
    Enumeration.mk_sort ctx
      (Symbol.mk_string ctx name)
      (List.map (Symbol.mk_int ctx) ints) in
  let sort1 = make_sort "sort1" [1; 2] in
  let ca = Expr.mk_fresh_const ctx "a" sort1 in
  let cb = Expr.mk_fresh_const ctx "b" sort1 in
  let cc = Expr.mk_fresh_const ctx "c" sort1 in
  let ( =/= ) a b = Boolean.mk_not ctx (Boolean.mk_eq ctx a b) in
  let check_and_report phs =
    match Solver.check solver phs with
    | Solver.UNKNOWN -> print_endline "unknown"
    | Solver.UNSATISFIABLE -> print_endline "UNSAT"
    | Solver.SATISFIABLE -> (
      match Solver.get_model solver with
      | None -> print_endline "no model"
      | Some m ->
          let get_var ca =
            Model.eval m ca false |> Option.get |> Expr.to_string in
          Format.printf "a = %s, b = %s, c = %s\n%!" (get_var ca) (get_var cc)
            (get_var cc) ) in
  let () = check_and_report [ca =/= cb; ca =/= cc] in
  let () = check_and_report [cb =/= cc] in
  let () = check_and_report [cb =/= cc; ca =/= cb; ca =/= cc] in
  ()
