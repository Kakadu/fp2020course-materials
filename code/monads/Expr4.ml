(** Copyright 2021-2023, Kakadu *)

(** SPDX-License-Identifier: LGPL-3.0-or-later *)

(* Generalization via monads  *)

type expr =
  | Const of int
  | Plus of expr * expr
  | Slash of expr * expr
  | Asterisk of expr * expr
  | Var of string

module type MONAD = sig
  type 'a t

  val return : 'a -> 'a t
  val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t

  module Syntax : sig
    (* A synonym for >>= *)
    val ( let* ) : 'a t -> ('a -> 'b t) -> 'b t
  end
end

module type MONADERROR = sig
  include MONAD

  val fail : string -> 'a t
end

module Eval (M : MONADERROR) = struct
  open M
  open M.Syntax

  let eval from_env : expr -> int M.t =
    let rec helper = function
      | Const n -> return n
      | Plus (l, r) ->
          let* l = helper l in
          let* r = helper r in
          return (l + r)
      | Asterisk (l, r) ->
          (* helper l >>= fun l -> *)
          let* l = helper l in
          let* r = helper r in
          return (l * r)
      | Slash (l, r) ->
          let* r = helper r in
          if r = 0 then fail "Division_by_zero"
          else
            let* l = helper l in
            return (l / r)
      | Var s -> from_env s
    in
    helper
end

module Option : MONADERROR with type 'a t = 'a option =
struct
  type 'a t = 'a option

  let ( >>= ) = Option.bind
  let return = Option.some
  let fail _ = None

  module Syntax = struct
    let ( let* ) = ( >>= )
  end
end

let%test_module _ =
  (module struct
    let run_option =
      let pp_option arg ppf = function
        | None -> Format.fprintf ppf "None"
        | Some x -> Format.fprintf ppf "(Some %a)" arg x
      in
      Format.printf "%a\n%!" (pp_option Format.pp_print_int)

    let%expect_test _ =
      let module E = Eval (Option) in
      run_option
      @@ E.eval
           (fun _ -> None)
           (Plus (Const 1, Asterisk (Const 2, Const 3)));
      [%expect {| (Some 7) |}]

    let%expect_test _ =
      let module E = Eval (Option) in
      run_option
      @@ E.eval (fun _ -> None) (Slash (Const 1, Const 0));
      [%expect {| None |}]
  end)

module Result :
  MONADERROR with type 'a t = ('a, string) Result.t = struct
  type 'a t = ('a, string) Result.t

  let ( >>= ) = Result.bind
  let return = Result.ok
  let fail = Result.error

  module Syntax = struct
    let ( let* ) = ( >>= )
  end
end

let%expect_test _ =
  let run_result =
    let pp ppf = function
      | Stdlib.Result.Error e ->
          Format.fprintf ppf "Error (%a)"
            Format.pp_print_string e
      | Ok x -> Format.fprintf ppf "%d" x
    in
    Format.printf "%a\n%!" pp
  in

  let module E = Eval (Result) in
  run_result
  @@ E.eval
       (fun s ->
         Error (Printf.sprintf "Variable %S is not found" s))
       (Var "x");
  [%expect {| Error (Variable "x" is not found) |}]

(* ********************** Lists ***************************************** *)
module L : MONADERROR with type 'a t = 'a list = struct
  type 'a t = 'a list

  let ( >>= ) x f = List.concat (List.map f x)
  let return x = [ x ]
  let fail _ = []

  module Syntax = struct
    let ( let* ) = ( >>= )
  end
end

let%expect_test _ =
  let module E = Eval (L) in
  List.sort Int.compare
    (E.eval
       (function "x" -> [ -1; 1 ] | _ -> [ -10; 10 ])
       (Plus (Var "x", Var "y")))
  |> List.map string_of_int
  |> String.concat "; " |> print_string;
  [%expect {| -11; -9; 9; 11 |}]

(* ********************** Identity ***************************************** *)
module ID : MONADERROR with type 'a t = 'a = struct
  type 'a t = 'a

  let ( >>= ) x f = f x
  let return x = x
  let fail = failwith

  module Syntax = struct
    let ( let* ) = ( >>= )
  end
end

let%expect_test _ =
  let module E = Eval (ID) in
  E.eval
    (function "x" -> 1 | _ -> raise Not_found)
    (Plus (Var "x", Const 1))
  |> print_int;
  [%expect {| 2 |}]

let%test _ =
  let module E = Eval (ID) in
  try
    let (_ : int) =
      E.eval
        (function "x" -> 1 | _ -> raise Not_found)
        (Slash (Var "x", Const 0))
    in
    false
  with
  | Failure s when s = "Division_by_zero" -> true
  | Failure _ -> false

(* ********************** Don't show below ********************************* *)
(* ********************** Continuation ************************************* *)

(*
fib : int  -> int
fibk : int -> (int -> 'a) -> 'a
*)
module Cont : sig
  include MONADERROR

  val run_cont : ('a -> 'b) -> 'a t -> 'b
end = struct
  type 'a t = { cont : 'b. ('a -> 'b) -> 'b }

  let return : 'a -> 'a t =
   fun x -> { cont = (fun k -> k x) }

  let ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t =
   fun x f ->
    { cont = (fun k -> x.cont (fun v -> (f v).cont k)) }

  let fail = failwith
  let run_cont k { cont } = cont k

  module Syntax = struct
    let ( let* ) = ( >>= )
  end
end

let%expect_test _ =
  let module E = Eval (Cont) in
  let env = function
    | "x" -> Cont.return 1
    | _ -> raise Not_found
  in
  Cont.run_cont Fun.id
    (E.eval env (Plus (Var "x", Const 1)))
  |> print_int;
  [%expect {| 2 |}]

(*
################
# 21112        #
####Я12 ########
#321112        #
# 32223456789  E
################

type front = cell list
type step = labyrinth -> cell list -> cell list

(* Returns reachable cells (not only front) *)
let search labyrinth  : cell list ->  cell list

if search l pos = set
then search l set === set

*)
