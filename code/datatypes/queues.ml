(* Adopted and changed from https://github.com/tianyicui/pfds-ocaml *)

module Stream = struct
  type 'a stream =
    | Nil
    | Cons of 'a * 'a stream Lazy.t

  type 'a t = 'a stream Lazy.t

  let rec of_list : _ list -> _ t = function
    | [] -> lazy Nil
    | x :: xs -> lazy (Cons (x, of_list xs))
  ;;

  let empty : _ t = lazy Nil

  let rec ( ++ ) : 'a t -> 'a t -> 'a t =
   fun xs ys ->
    match xs with
    | (lazy Nil) -> ys
    | (lazy (Cons (h, tl))) -> lazy (Cons (h, tl ++ ys))
 ;;

  let reverse : 'a t -> 'a t =
    let rec helper acc = function
      | (lazy Nil) -> acc
      | (lazy (Cons (h, tl))) -> helper (lazy (Cons (h, acc))) tl
    in
    fun s -> lazy (Lazy.force (helper (lazy Nil) s))
  ;;
end

exception Empty

module type QUEUE = sig
  type 'a queue

  val empty : 'a queue
  val is_empty : 'a queue -> bool
  val snoc : 'a queue -> 'a -> 'a queue
  val head : 'a queue -> 'a (* raises Empty if queue is empty *)
  val tail : 'a queue -> 'a queue (* raises Empty if queue is empty *)
  val of_list : 'a list -> 'a queue
end

module BatchedQueue : QUEUE = struct
  type 'a queue = 'a list * 'a list

  let empty = [], []
  let is_empty (f, _) = f = []

  let checkf = function
    | [], r -> List.rev r, []
    | q -> q
  ;;

  let snoc (f, r) x = checkf (f, x :: r)

  let head = function
    | [], _ -> raise Empty
    | x :: _, _ -> x
  ;;

  let tail = function
    | [], _ -> raise Empty
    | _ :: f, r -> checkf (f, r)
  ;;

  let of_list xs = xs, []
end

module BankersQueue : QUEUE = struct
  type 'a queue = int * 'a Stream.t * int * 'a Stream.t

  open Stream

  let empty = 0, lazy Nil, 0, lazy Nil
  let is_empty (lenf, _, _, _) = lenf = 0

  let check ((lenf, f, lenr, r) as q) =
    if lenr <= lenf then q else lenf + lenr, f ++ reverse r, 0, lazy Nil
  ;;

  let snoc (lenf, f, lenr, r) x = check (lenf, f, lenr + 1, lazy (Cons (x, r)))

  let head = function
    | _, (lazy Nil), _, _ -> raise Empty
    | _, (lazy (Cons (x, _))), _, _ -> x
  ;;

  let tail = function
    | _, (lazy Nil), _, _ -> raise Empty
    | lenf, (lazy (Cons (_, f'))), lenr, r -> check (lenf - 1, f', lenr, r)
  ;;

  let of_list xs = List.length xs, Stream.of_list xs, 0, Stream.empty
end

module RealTimeQueue : QUEUE = struct
  open Stream

  type 'a queue = 'a Stream.t * 'a list * 'a Stream.t

  let empty = lazy Nil, [], lazy Nil

  let is_empty = function
    | (lazy Nil), _, _ -> true
    | _ -> false
  ;;

  let rec rotate = function
    | (lazy Nil), y :: _, a -> lazy (Cons (y, a))
    | (lazy (Cons (x, xs))), y :: ys, a ->
      lazy (Cons (x, rotate (xs, ys, lazy (Cons (y, a)))))
    | _ -> failwith "unreachable"
  ;;

  let exec = function
    | f, r, (lazy (Cons (_, s))) -> f, r, s
    | f, r, (lazy Nil) ->
      let f' = rotate (f, r, lazy Nil) in
      f', [], f'
  ;;

  let snoc (f, r, s) x = exec (f, x :: r, s)

  let head = function
    | (lazy Nil), _, _ -> raise Empty
    | (lazy (Cons (x, _))), _, _ -> x
  ;;

  let tail = function
    | (lazy Nil), _, _ -> raise Empty
    | (lazy (Cons (_, f))), r, s -> exec (f, r, s)
  ;;

  let of_list xs = List.fold_left snoc empty xs

  let%test _ =
    let q = snoc empty 1 in
    not (is_empty q)
  ;;
end
