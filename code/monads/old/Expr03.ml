(* interpreter with errors *)

open Expr0

(* Base case *)
type expr =
  | Const of int
  | Plus of expr * expr
  | Asterisk of expr * expr
  | Slash of expr * expr
  | Var of string

module L = struct
  let c n = Const n
  let v n = Var n
  let ( + ) a b = Plus (a, b)
  let ( * ) a b = Asterisk (a, b)
  let ( / ) a b = Slash (a, b)
end

let rec eval lookup = function
  | Const n -> n
  | Plus (l, r) -> eval lookup l + eval lookup r
  | Asterisk (l, r) -> eval lookup l * eval lookup r
  | Slash (l, r) -> eval lookup l / eval lookup r
  | Var s -> lookup s
;;

let%test _ = 7 = eval (fun _ -> assert false) L.(c 1 + (c 2 * c 3))
let%test _ = 3 = eval (fun s -> List.assoc s [ "x", 2 ]) L.(c 1 + v "x")

module Eval (M : APPLICATIVE) = struct
  open M

  let rec eval lookup = function
    | Const n -> pure n
    | Plus (l, r) -> pure ( + ) <*> eval lookup l <*> eval lookup r
    | Asterisk (l, r) -> pure ( + ) <*> eval lookup l <*> eval lookup r
    | Slash (l, r) -> pure ( / ) <*> eval lookup l <*> eval lookup r
    | Var s -> lookup s
  ;;
end

(*
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *)

type error =
  [ `DivisionByZero
  | `UnboundVariable
  ]

module _ = struct
  module Rez = struct
    type 'a t = ('a, error) Result.t

    let pure x = Result.ok x
    let fail = Result.error
    let fmap = Result.map
    let ( >>| ) xs f = fmap f xs

    let ( <*> ) : 'a 'b. ('a -> 'b) t -> 'a t -> 'b t =
     fun fs xs ->
      match fs, xs with
      | Result.Ok f, Result.Ok x -> Result.Ok (f x)
      | Result.Error e, _ | _, Result.Error e -> fail e
   ;;

    module Syntax = struct
      let ( let+ ) = ( >>| )
    end
  end

  (* attempt 0 *)
  module Eval (M : APPLICATIVE) = struct
    open M

    let rec eval lookup = function
      | Const n -> pure n
      | Plus (l, r) -> pure ( + ) <*> eval lookup l <*> eval lookup r
      | Asterisk (l, r) -> pure ( + ) <*> eval lookup l <*> eval lookup r
      | Slash (l, r) -> pure ( / ) <*> eval lookup l <*> eval lookup r
      | Var s -> lookup s
    ;;
  end

  module E = Eval (Rez)
  open E

  let%test _ =
    Rez.fail `UnboundVariable = eval (fun _ -> Rez.fail `UnboundVariable) L.(c 1 + v "x")
  ;;
end

module _ = struct
  module type ARG = sig
    include APPLICATIVE

    val fail : error -> 'a t
  end

  module Eval (M : ARG) = struct
    open M

    let rec eval lookup = function
      | Const n -> pure n
      | Plus (l, r) -> pure ( + ) <*> eval lookup l <*> eval lookup r
      | Asterisk (l, r) -> pure ( + ) <*> eval lookup l <*> eval lookup r
      | Slash (l, r) ->
        pure (fun x y -> if y = 0 then assert false else x / y)
        <*> eval lookup l
        <*> eval lookup r
      | Var s -> lookup s
    ;;
  end
end [@ocaml.warnerror "-32"] [@ocaml.warning "-32"]

module type MONAD = sig
  type 'a t

  include APPLICATIVE with type 'a t := 'a t

  val return : 'a -> 'a t
  val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t

  module Syntax : sig
    val ( let* ) : 'a t -> ('a -> 'b t) -> 'b t
    val ( let+ ) : 'a t -> ('a -> 'b) -> 'b t
    val ( and+ ) : 'a t -> 'b t -> ('a * 'b) t
  end
end

module Rez = struct
  type 'a t = ('a, error) Result.t

  let pure x = Result.Ok x
  let fail = Result.error
  let fmap = Result.map
  let ( >>| ) xs f = fmap f xs

  (* let ( <*> ) : 'a 'b. ('a -> 'b) t -> 'a t -> 'b t =
   fun fs xs ->
    match fs, xs with
    | Result.Ok f, Result.Ok x -> Result.Ok (f x)
    | Result.Error e, _ | _, Result.Error e -> fail e
 ;; *)

  let ( >>= ) = Result.bind
  let return = Result.ok

  let both a b =
    match a, b with
    | Result.Ok l, Result.Ok r -> Result.Ok (l, r)
    | Error e, _ | _, Error e -> Error e
  ;;

  let ( <*> ) fx gx = both fx gx >>| fun (f, x) -> f x
  (* match both fx gx with
    | Result.Error e -> fail e
    | Ok (f, x) -> pure (f x)
  ;; *)

  module Syntax = struct
    let ( let+ ) = ( >>| )
    let ( and+ ) = both
    let ( let* ) = ( >>= )
  end
end

module _ = struct
  module type ARG = sig
    include MONAD

    val fail : error -> 'a t
  end

  module Eval (M : ARG) = struct
    open M
    open M.Syntax

    let rec eval lookup = function
      | Const n -> pure n
      | Plus (l, r) -> pure ( + ) <*> eval lookup l <*> eval lookup r
      | Asterisk (l, r) ->
        let+ l = eval lookup l
        and+ r = eval lookup r in
        l + r
      | Slash (l, r) ->
        let* rhs = eval lookup r in
        if rhs = 0
        then fail `DivisionByZero
        else
          let* lhs = eval lookup l in
          return (lhs / rhs)
      | Var s -> lookup s
    ;;
  end

  module E = Eval (Rez)
  open E

  let%test _ =
    Rez.fail `UnboundVariable = eval (fun _ -> Rez.fail `UnboundVariable) L.(c 1 + v "x")
  ;;

  let%test _ =
    Rez.fail `DivisionByZero = eval (fun _ -> Rez.fail `UnboundVariable) L.(c 1 / c 0)
  ;;
end
(*
(*
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *)
module _ = struct
  module type ARG = sig
    include MONAD

    val get_lookup : (string -> int t) t
  end

  module Eval (M : ARG) = struct
    open M
    open M.Syntax

    let rec eval = function
      | Const n -> pure n
      | Plus (l, r) -> pure ( + ) <*> eval l <*> eval r
      | Asterisk (l, r) -> pure ( + ) <*> eval l <*> eval r
      | Slash (l, r) -> pure ( / ) <*> eval l <*> eval r
      | Var s ->
        let* lookup = get_lookup in
        lookup s
    ;;
  end

  type env = (string * int) list

  module Arg = struct
    type 'a t = env -> 'a

    let ( >>= ) : 'a 'b. 'a t -> ('a -> 'b t) -> 'b t = fun x f env -> f (x env) env
    let return x _ = x
    let ( >>| ) x f env = f (x env)
    let ( <*> ) f x env = f env (x env)
    let pure = return

    module Syntax = struct
      include Expr0.Id.Syntax

      let ( let* ) = ( >>= )
    end

    let get_lookup : (string -> 'a t) t = fun env s _ -> List.assoc s env
    let run env m = m env
  end

  let%test _ =
    let module E = Eval (Arg) in
    let open E in
    3 = Arg.run [ "x", 2 ] (eval L.(c 1 + v "x"))
  ;;
end *)
