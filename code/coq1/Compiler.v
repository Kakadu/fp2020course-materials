Require Import Nat.
Require Import List.
Import ListNotations.

Inductive binop := Plus | Asterisk.
Inductive expr :=
  | Const : nat -> expr
  | BinOp : binop -> expr -> expr -> expr
  .

Fixpoint aeval e :=
  match e with
  | Const n => n
  | BinOp Plus l r => add (aeval l) (aeval r)
  | BinOp Asterisk l r => mul (aeval l) (aeval r)
  end.

Eval cbv in aeval (BinOp Plus (Const 1) (Const 10)).
Eval cbv in aeval (BinOp Asterisk (Const 2) (Const 3)).

Inductive instr :=
  | Int : nat -> instr
  | Cmd : binop -> instr.
Search list.

Fixpoint s_compile (e : expr) : list instr :=
  match e with 
    Const n => [ Int n ]
  | BinOp op l r =>  s_compile l ++ s_compile r ++ [Cmd op]
  end.

(* https://softwarefoundations.cis.upenn.edu/lf-current/Imp.html *)

Fixpoint eval_stack (program: list instr) stack : list nat :=
  match program, stack with
  | [], _ => stack
  | Int n :: ps, _ => eval_stack ps (n :: stack)
  | Cmd Plus :: ps, x::y::ss => eval_stack ps ((add x y) :: ss)
  | Cmd Asterisk :: ps, x::y::ss => eval_stack ps ((mul x y) :: ss)
  | Cmd _ :: ps, _ => eval_stack ps stack
  end.

Eval cbv in eval_stack [Int 5] [ 6 ].
Eval cbv in eval_stack [Int 5; Int 6; Cmd Plus] [ 42 ].
Eval cbv in eval_stack (s_compile (BinOp Plus (Const 1) (Const 2))) [].

Theorem execute_app : forall p1 p2 stack,
  eval_stack (p1 ++ p2) stack = eval_stack p2 (eval_stack p1 stack).
Proof.
  intros p1. induction p1.
  * simpl. reflexivity.
  * intros p2 stack. rewrite <- (app_comm_cons p1 p2 a).
    induction a.
    + simpl. rewrite  (IHp1 p2 (n::stack)). reflexivity.
    + induction b. unfold eval_stack. fold eval_stack.
      destruct stack. 
      - rewrite (IHp1 p2 []). reflexivity.
      - destruct stack.
        -- rewrite (IHp1 p2 [n]); reflexivity.
        -- rewrite (IHp1 p2 ((n+n0) :: stack)); reflexivity.
   - unfold eval_stack. fold eval_stack.
      destruct stack. 
      -- rewrite (IHp1 p2 []). reflexivity.
      -- destruct stack.
        --- rewrite (IHp1 p2 [n]); reflexivity.
        --- rewrite (IHp1 p2 ((n * n0) :: stack)); reflexivity.
Qed.

Lemma s_compile_correct_aux : forall e stack,
  eval_stack (s_compile e) stack = aeval e :: stack.
Proof.
  induction e. 
  + unfold s_compile. unfold aeval.   unfold eval_stack. reflexivity.
  + induction b. 
    - unfold s_compile. fold s_compile.
    intros stack.
    rewrite (execute_app (s_compile e1) (s_compile e2 ++ [Cmd Plus])).
    rewrite (execute_app (s_compile e2) [Cmd Plus]).
    rewrite (IHe1 stack). rewrite (IHe2 (aeval e1 :: stack)).
    unfold eval_stack.
    unfold aeval. fold aeval.
    rewrite PeanoNat.Nat.add_comm. reflexivity.
    - unfold s_compile. fold s_compile.
    intros stack.
    rewrite (execute_app (s_compile e1) (s_compile e2 ++ [Cmd Asterisk])).
    rewrite (execute_app (s_compile e2) [Cmd Asterisk]).
    rewrite (IHe1 stack). rewrite (IHe2 (aeval e1 :: stack)).
    unfold eval_stack.
    unfold aeval. fold aeval.
    rewrite PeanoNat.Nat.mul_comm. reflexivity.
Qed.

Theorem s_compile_correct : forall (e : expr),
  eval_stack  (s_compile e) [] = [ aeval e ].
Proof.
  intros e.
  rewrite (s_compile_correct_aux e []). reflexivity.
Qed.

(* *********************** *)
Require Extraction.
Extraction Language OCaml.

Extract Inductive bool => "bool" [ "true" "false" ].
Extract Inductive list => "list" [ "[]" "(::)" ].
Extraction "Compiler.ml"  eval_stack aeval.
