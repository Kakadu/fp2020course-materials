module _ = struct
  module S1 = Set.Make (struct
    type t = int

    let compare = compare
  end)

  module S2 = Set.Make (struct
    type t = int

    let compare = compare
  end)

  let __ _ = S1.add 42 S1.empty

  (*
  Error: This expression has type S2.t but an expression was expected of type
         S1.t
  *)
  (* let __ _ = S1.add 42 S2.empty *)
end

module _ = struct
  module Key = struct
    type t = int

    let compare = compare
  end

  module S1 = Set.Make (Key)
  module S2 = Set.Make (Key)

  let __ _ = S1.add 42 S1.empty

  (* fine! *)
  let __ _ = S1.add 42 S2.empty
end

module _ = struct
  open Base

  let _ =
    let s1 = Set.add (Set.empty (module Int)) 42 in
    let s2 = Set.add (Set.empty (module Int)) 42 in
    Set.union s1 s2 |> Set.to_list

  module Cmp2 = struct
    module T = struct
      type t = int [@@deriving compare, sexp_of]

      let sexp_of_t = Int.sexp_of_t
      let compare a b = Int.compare (Int.rem a 128) (Int.rem b 128)
    end

    include T
    include Comparator.Make (T)
  end
  (*
  let _ =
    let s1 = Set.add (Set.empty (module Cmp2)) 42 in
    let s2 = Set.add (Set.empty (module Int)) 42 in
    Set.union s1 s2 |> Set.to_list *)
end
