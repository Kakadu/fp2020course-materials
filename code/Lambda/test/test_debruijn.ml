open Lambda_lib.Lambda
open Lambda_lib.Pprintast

let one = abs "f" (abs "x" (app (var "f") (var "x")))

let%expect_test _ =
  Format.printf "%a" pp_nameless (to_debruijn_level_exn one);
  [%expect {| (λ.(λ.(0 1))) |}]
;;
