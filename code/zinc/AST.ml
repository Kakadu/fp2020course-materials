type op = Add | Mul

type t =
  | Var of int
  | Abs of t
  | App of t * t
  | Const of int
  | Binop of op * t * t
  | Let of t * t
