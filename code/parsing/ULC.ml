[@@@ocaml.text "/*"]

(** Copyright 2021-2023, Kakadu *)

(** SPDX-License-Identifier: LGPL-3.0-or-later *)

[@@@ocaml.text "/*"]

open Angstrom
open Format

module Lam = struct
  type t = Var of char | App of t * t | Abs of char * t
  [@@deriving show { with_path = false }]

  let var x = Var x
  let abs x l = Abs (x, l)
  let app l r = App (l, r)
end

let is_space = function ' ' | '\t' -> true | _ -> false
let spaces = skip_while is_space
let varname = satisfy (function 'a' .. 'z' -> true | _ -> false)

let conde = function
  | [] -> fail "empty conde"
  | h :: tl -> List.fold_left ( <|> ) h tl

type dispatch = {
  apps : dispatch -> Lam.t Angstrom.t;
  single : dispatch -> Lam.t Angstrom.t;
}

let parse_lam =
  let single pack =
    fix (fun _ ->
        conde
          [
            char '(' *> pack.apps pack <* char ')';
            ( (string "λ" <|> string "\\") *> spaces *> varname
            <* spaces <* char '.'
            >>= fun var ->
              pack.apps pack >>= fun b -> return (Lam.Abs (var, b)) );
            (varname <* spaces >>= fun c -> return (Lam.Var c));
          ])
  in
  let apps pack =
    many1 (spaces *> pack.single pack <* spaces) >>= function
    | [] -> fail "bad syntax"
    | x :: xs -> return @@ List.fold_left (fun l r -> Lam.App (l, r)) x xs
  in
  { single; apps }

let parse_optimistically str =
  Result.get_ok
  @@ Angstrom.parse_string (parse_lam.apps parse_lam) str
       ~consume:Angstrom.Consume.All

let%expect_test _ =
  Format.printf "%a" Lam.pp (parse_optimistically "a a   ");
  [%expect {| App (Var (a), Var (a)) |}]

let%expect_test _ =
  Format.printf "%a" Lam.pp (parse_optimistically "x y");
  [%expect {| App (Var (x), Var (y)) |}]

let%expect_test _ =
  Format.printf "%a" Lam.pp (parse_optimistically "(x y)");
  [%expect {| App (Var (x), Var (y)) |}]

let%expect_test _ =
  Format.printf "%a" Lam.pp (parse_optimistically "(\\x . x x)");
  [%expect {| Abs (x, App (Var (x), Var (x))) |}]

let%expect_test _ =
  Format.printf "%a" Lam.pp (parse_optimistically "(λf.λx. f (x x))");
  [%expect {| Abs (f, Abs (x, App (Var (f), App (Var (x), Var (x))))) |}]

module PP = struct
  let rec pp ppf = function
    | Lam.Var c -> Format.fprintf ppf "%c" c
    | App (l, r) ->
        Format.fprintf ppf "(%a %a)" pp l pp r
        (* | App (l, r) ->
            Format.fprintf ppf "%a %a" pp l pp r *)
        (* Buggy implementation *)
    | Abs (x, b) -> Format.fprintf ppf "(\\%c . %a)" x pp b

  (* let pp ppf x = fprintf ppf "`%a`" pp x *)
end

module TestQCheck = struct
  open QCheck.Gen

  (** Manual generator *)

  let varname = map Char.chr (int_range (Char.code 'a') (Char.code 'z'))

  let lam_gen_manual =
    QCheck.Gen.(
      sized
      @@ fix (fun self n ->
             match n with
             | 0 -> map Lam.var varname
             | n ->
                 frequency
                   [
                     (1, map2 Lam.abs varname (self (n / 2)));
                     (1, map2 Lam.app (self (n / 2)) (self (n / 2)));
                   ]))

  let rec shrink_lam =
    let open QCheck.Iter in
    function
    | Lam.Var i -> QCheck.Shrink.char i >|= Lam.var
    | Abs (c, b) -> of_list [ b ] <+> (shrink_lam b >|= fun b' -> Lam.abs c b')
    | App (a, b) ->
        of_list [ a; b ]
        <+> (shrink_lam a >|= fun a' -> Lam.app a' b)
        <+> (shrink_lam b >|= fun b' -> Lam.app a b')

  let arbitrary_lam_manual =
    QCheck.make lam_gen_manual ~print:(asprintf "%a" PP.pp) ~shrink:shrink_lam

  (** Generator constructed via deriving  *)

  include (
    struct
      type t = Lam.t =
        | Var of (char[@gen varname])
        | App of t * t
        | Abs of (char[@gen varname]) * t
      [@@deriving qcheck]
    end :
      sig
        (* val gen_sized : int -> Lam.t QCheck.Gen.t *)
        val gen : Lam.t QCheck.Gen.t
        (* val arb_sized : int -> Lam.t QCheck.arbitrary *)
        (* val arb : Lam.t QCheck.arbitrary *)
      end)

  let arbitrary_lam_auto =
    let open QCheck.Iter in
    QCheck.make gen ~print:(asprintf "%a" PP.pp) ~shrink:shrink_lam

  let run_manual () =
    QCheck_runner.run_tests
      [
        QCheck.(
          Test.make arbitrary_lam_manual (fun l ->
              Result.ok l
              = Angstrom.parse_string ~consume:Consume.All
                  (parse_lam.apps parse_lam)
                  (Format.asprintf "%a" PP.pp l)));
      ]

  let run_auto () =
    QCheck_runner.run_tests
      [
        QCheck.(
          Test.make arbitrary_lam_auto (fun l ->
              match
                Angstrom.parse_string ~consume:Consume.All
                  (parse_lam.apps parse_lam)
                  (Format.asprintf "%a" PP.pp l)
              with
              | Result.Ok after when after = l -> true
              | Result.Ok _after ->
                  (* Format.printf "before : %a\n%!" Lam.pp l;
                     Format.printf "       : `%a`\n%!" PP.pp l;
                     Format.printf "`%a`\n%!" PP.pp after; *)
                  false
              | Result.Error _ ->
                  (* Format.printf "failed on : %a\n%!" Lam.pp l; *)
                  false));
      ]
end
