let run input =
  let open Parser in
  let ( let* ) = Result.bind in
  let* f = Parser.parse_formula input in
  let rec loop axioms targets =
    let () =
      match targets with
      | [] -> Format.printf "No more targets.\n%!"
      | [ h ] ->
          List.iteri
            (fun i ph -> Format.printf "%3d: %a\n" i Prop.pp_expr ph)
            axioms;
          Format.printf "==========================\n%!";
          Format.printf "  %a\n\n%!" Prop.pp_expr h
      | h :: tl ->
          List.iteri
            (fun i ph -> Format.printf "%3d: %a\n" i Prop.pp_expr ph)
            axioms;
          Format.printf "==========================\n%!";
          Format.printf "  %a\n\n%!" Prop.pp_expr h;
          if [] <> tl then
            Printf.printf "There are %d more targets" (List.length tl)
    in
    match In_channel.(input_line stdin) with
    | None -> Result.ok ()
    | Some s -> (
        let open Parser.User_cmd in
        match (Parser.parse_cmd s, targets) with
        | (Error _ as e), _ -> e
        | Ok Qed, [] -> Result.ok ()
        | Ok Intros, Prop.Binop (Prop.Impl, l, r) :: targets ->
            loop (l :: axioms) (r :: targets)
        | Ok Axiom, th :: ttl ->
            if List.mem th axioms then loop axioms ttl
            else (
              Printf.printf "No such axiom.\n";
              loop axioms targets)
        | Ok _, _ ->
            print_endline s;
            Format.(eprintf "target = %a\n" (pp_print_list Prop.pp_expr))
              targets;
            failwith "command not implemented")
  in
  loop [] [ f ]

let () =
  match run Sys.argv.(1) with
  | Result.Ok _ -> ()
  | Error s -> Printf.eprintf "%s\n" s
