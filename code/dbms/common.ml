type exc = string

module type MONAD = sig
  type 'a t

  val return : 'a -> 'a t
  val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t
end

module type MONADFAIL = sig
  include MONAD

  val fail : string -> 'a t
end

module type TRANSFORMER = sig
  type 'a m
  type 'a t

  val promote : 'a m -> 'a t
  val observe : 'a t -> 'a m
end

module type MAKE_TRANSFORMER = functor (M : MONADFAIL) -> sig
  include TRANSFORMER with type 'a m = 'a M.t
  include MONADFAIL with type 'a t := 'a t
end

(*
  Законы P1 & P2
  Законы O1 & O2
*)

module Identity : MONADFAIL with type 'a t = 'a = struct
  type 'a t = 'a

  let return x = x
  let ( >>= ) x f = f x
  let fail = failwith
end
