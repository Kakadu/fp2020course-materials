(** Copyright 2021-2023, Kakadu *)

(** SPDX-License-Identifier: LGPL-3.0-or-later *)

(* State/Reader/Writer *)
(* TODO: сказать когда какая монада нужна *)

type expr =
  | Const of int
  | Plus of expr * expr
  | Asterisk of expr * expr
  | Var of string

module type MONAD = sig
  type 'a t

  val return : 'a -> 'a t
  val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t

  module Syntax : sig
    (* A synonym for >>= *)
    val ( let* ) : 'a t -> ('a -> 'b t) -> 'b t
  end
end

module type MONAD_ERROR = sig
  include MONAD

  val fail : string -> 'a t
end

module Eval1 (M : MONAD_ERROR) = struct
  open M
  open M.Syntax

  let eval from_env : expr -> int M.t =
    let rec helper = function
      | Const n -> return n
      | Plus (l, r) -> helper_plus from_env l r
      | Asterisk (l, r) -> helper_asterisk from_env l r
      | Var s -> from_env s
    and helper_plus _from_env l r =
      let* l = helper l in
      let* r = helper r in
      return (l + r)
    and helper_asterisk _from_env l r =
      let* l = helper l in
      let* r = helper r in
      return (l * r)
    in
    helper
end

module Option = struct
  type 'a t = 'a option

  let ( >>= ) = Option.bind
  let return = Option.some
  let fail _ = None

  module Syntax = struct
    let ( let* ) = ( >>= )
  end
end

let%test _ =
  let module E = Eval1 (Option) in
  Some 7
  = E.eval
      (fun _ -> None)
      (Plus (Const 1, Asterisk (Const 2, Const 3)))

module type READER_MONAD = sig
  type ('s, 'a) t

  val return : 'a -> ('s, 'a) t

  val ( >>= ) :
    ('s, 'a) t -> ('a -> ('s, 'b) t) -> ('s, 'b) t

  val read : ('s, 's) t
  val run : ('st, 'a) t -> 'st -> 'a

  module Syntax : sig
    (* A synonym for >>= *)
    val ( let* ) :
      ('s, 'a) t -> ('a -> ('s, 'b) t) -> ('s, 'b) t
  end
end

module Eval2 (M : READER_MONAD) = struct
  open M
  open M.Syntax

  let eval : expr -> (string -> int, int) M.t =
    let rec helper = function
      | Const n -> return n
      | Plus (l, r) -> helper_plus l r
      | Asterisk (l, r) -> helper_asterisk l r
      | Var s ->
          let* from_env = read in
          return (from_env s)
    and helper_plus l r =
      let* l = helper l in
      let* r = helper r in
      return (l + r)
    and helper_asterisk l r =
      let* l = helper l in
      let* r = helper r in
      return (l * r)
    in
    helper
end

module Simple_reader : READER_MONAD = struct
  type ('st, 'a) t = 'st -> 'st * 'a

  let ( >>= ) :
        's 'a 'b.
        ('s, 'a) t -> ('a -> ('s, 'b) t) -> ('s, 'b) t =
   fun m f : _ ->
    fun st ->
     let st, x = m st in
     f x st

  let return : 'a 's. 'a -> ('s, 'a) t = fun x st -> (st, x)
  let read : ('st, 'st) t = fun st -> (st, st)

  let run : ('st, 'a) t -> 'st -> 'a =
   fun f st -> snd (f st)

  module Syntax = struct
    let ( let* ) = ( >>= )
  end
end

let%test _ =
  let module E = Eval2 (Simple_reader) in
  let expr = Plus (Const 1, Asterisk (Const 2, Const 3)) in
  7 = Simple_reader.run (E.eval expr) (fun _ -> 42)

module type WRITER_MONAD = sig
  type ('s, 'a) t

  val return : 'a -> ('s, 'a) t

  val ( >>= ) :
    ('s, 'a) t -> ('a -> ('s, 'b) t) -> ('s, 'b) t

  val write : 's -> ('s, unit) t
  val run : ('st, 'a) t -> 'st list * 'a

  module Syntax : sig
    (* A synonym for >>= *)
    val ( let* ) :
      ('s, 'a) t -> ('a -> ('s, 'b) t) -> ('s, 'b) t
  end
end

(* A la logger *)
module Simple_writer : WRITER_MONAD = struct
  type ('st, 'a) t = 'st list * 'a

  let ( >>= ) :
        's 'a 'b.
        ('s, 'a) t -> ('a -> ('s, 'b) t) -> ('s, 'b) t =
   fun (log1, x) f : _ ->
    let log2, rez = f x in
    (log1 @ log2, rez)

  let return : 'a -> ('st, 'a) t = fun x -> ([], x)
  let write : 'st -> ('st, unit) t = fun x -> ([ x ], ())
  let run : ('st, 'a) t -> 'st list * 'a = Fun.id

  module Syntax = struct
    let ( let* ) = ( >>= )
  end
end

module Eval3 (M : WRITER_MONAD) = struct
  open M
  open M.Syntax

  let eval : expr -> (string, int) M.t =
    let rec helper = function
      | Const n -> return n
      | Plus (l, r) ->
          let* l = helper l in
          let* r = helper r in
          let* () = write "Addition" in
          return (l + r)
      | Asterisk (l, r) ->
          let* l = helper l in
          let* r = helper r in
          let* () = write "Multiplication" in
          return (l * r)
      | Var _ -> failwith "not important for this example"
    in
    helper
end

let%test _ =
  let module E = Eval3 (Simple_writer) in
  let expr = Plus (Const 1, Asterisk (Const 2, Const 3)) in
  match Simple_writer.run (E.eval expr) with
  | text, rez ->
      text = [ "Multiplication"; "Addition" ] && rez = 7

module type STATE_MONAD = sig
  include READER_MONAD

  val write : 's -> ('s, unit) t
  val run : ('st, 'a) t -> 'st -> 'st * 'a
end

module Simple_state : STATE_MONAD = struct
  type ('st, 'a) t = 'st -> 'st * 'a

  let ( >>= ) :
        's 'a 'b.
        ('s, 'a) t -> ('a -> ('s, 'b) t) -> ('s, 'b) t =
   fun x f : _ ->
    fun st ->
     let st, x = x st in
     f x st

  let return x : _ = fun st -> (st, x)
  let read : ('st, 'st) t = fun st -> (st, st)

  let write : 'st -> ('st, unit) t =
   fun s _oldstate -> (s, ())

  let run : ('st, 'a) t -> 'st -> 'st * 'a =
   fun f st -> f st

  module Syntax = struct
    let ( let* ) = ( >>= )
  end
end

(* Calculated via stack and recursively at the same time *)
module Eval4 (M : STATE_MONAD) = struct
  open M
  open M.Syntax

  let eval : expr -> (int list, int) M.t =
    let rec helper = function
      | Const n ->
          let* stack = read in
          let* () = write (n :: stack) in
          return n
      | Plus (l, r) -> (
          let* l = helper l in
          let* r = helper r in
          let* stack = read in
          match stack with
          | arg1 :: arg2 :: rest ->
              let* () = write ((arg1 + arg2) :: rest) in
              return (l + r)
          | _ -> failwith "report error using Error monad")
      | Asterisk (l, r) -> (
          let* l = helper l in
          let* r = helper r in
          let* stack = read in
          match stack with
          | arg1 :: arg2 :: rest ->
              let* () = write ((arg1 * arg2) :: rest) in
              return (l * r)
          | _ -> failwith "report error using Error monad")
      | Var _ -> failwith "Not important for this example"
    in
    helper
end

let%test _ =
  let module E = Eval4 (Simple_state) in
  let expr = Plus (Const 1, Asterisk (Const 2, Const 3)) in
  match Simple_state.run (E.eval expr) [] with
  | stack, rez -> stack = [ 7 ] && rez = 7
