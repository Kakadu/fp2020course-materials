(** Copyright 2021-2023, Kakadu *)

(** SPDX-License-Identifier: LGPL-3.0-or-later *)

(* Errors via failwith *)

(* TODO: explain let-polymorphsm *)
let%test_module _ =
  (module struct
    (* Idea. In some sence
            let x = A in B
        should be the same as
            (fun x -> B) A
    *)
    let%test "naive good" =
      let twice f x = f (f x) in
      twice not true = true

    let%test "meaningful good" =
      let twice f x = f (f x) in
      (twice not true, twice (( + ) 1) 4) = (true, 6)

    let%test "naive OK" =
      (fun twice -> twice not true) (fun f x -> f (f x))
      = true

    (*
      (fun twice -> (twice (( +. ) 1.) 4., twice not true))

      type tt = { field : forall 'a . ('a -> 'a) -> 'a -> 'a) }
      tt -> (float, bool)
*)

    (* let%test "naive. doesn't compile" =
       (fun twice -> (twice (( +. ) 1.) 4., twice not true))
         (fun f x -> f (f x))
       = (true, 6.) *)
  end)

type expr =
  | Const of int
  | Plus of expr * expr
  | Slash of expr * expr
  | Asterisk of expr * expr
  | Var of string

let eval from_env : expr -> int option =
  let ( let* ) x f =
    match x with None -> None | Some x -> f x
  in
  let return x = Some x in
  let fail _ = None in
  let rec helper = function
    | Const n -> return n
    | Plus (l, r) ->
        let* l = helper l in
        let* r = helper r in
        return (l + r)
    | Asterisk (l, r) ->
        let* l = helper l in
        let* r = helper r in
        return (l * r)
    | Slash (l, r) ->
        let* r = helper r in
        if r = 0 then fail "division by zero"
        else
          let* l = helper l in
          return (l / r)
    | Var s -> from_env s
  in
  helper

let run_and_pp x =
  let pp_option arg ppf = function
    | None -> Format.fprintf ppf "None"
    | Some x -> Format.fprintf ppf "(Some %a)" arg x
  in
  Format.printf "%a\n%!" (pp_option Format.pp_print_int) x

let%expect_test _ =
  run_and_pp
  @@ eval
       (fun _ -> None)
       (Plus (Const 1, Asterisk (Const 2, Const 3)));
  [%expect {| (Some 7) |}]

let%expect_test _ =
  run_and_pp @@ eval (fun _ -> None) (Var "x");
  [%expect {| None |}]

let%expect_test _ =
  run_and_pp
  @@ eval (fun _ -> None) (Slash (Const 1, Const 0));
  [%expect {| None |}]
