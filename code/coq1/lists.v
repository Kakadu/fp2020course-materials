Inductive peano  :=
  | Zero : peano
  | Succ : peano -> peano.

Print peano_rec.
Inductive lst {A: Type} :=
  | Nil : lst
  | Cons : A -> lst -> lst.

Print lst_rec.

Fixpoint append {A: Type} (xs ys: lst) : @lst A :=
  match xs with
  | Nil => ys
  | Cons h tl => Cons h (append tl ys)
  end.

Notation "[ ]" := Nil .
Notation "[ x ]" := (Cons x []).
Notation "[ x ; y ; .. ; z ]" := (Cons x (Cons y .. (Cons z []) ..)).
Infix "::" := Cons (at level 60, right associativity) : list_scope.

Eval cbv in  [1;2;3].

Eval cbv in append [1;2;3] [4].

Notation "x +++ y" := (append x y)
                      (at level 61, left associativity).

Eval cbv in [1;2;3] +++ [4].


Theorem app_assoc : forall {A: Type} (l1 l2 l3 : @lst A),
  (l1 +++ l2) +++ l3 = l1 +++ (l2 +++ l3).
Proof.
  intros A l1 l2 l3.
  induction l1 as [| h tl Hypo ].
  + simpl. reflexivity.
  + simpl. rewrite Hypo. reflexivity.
Qed.

Fixpoint rev {A: Type} (l: lst) : @lst A :=
  match l with
    | [] => []
    | Cons x l' => rev l' +++ [x]
  end.

Eval cbv in rev [1;2;3].

  Lemma app_right_neutral {A: Type}: forall (xs: lst),
    @append A xs [] = xs.
  Proof.
    induction xs.
    * simpl. reflexivity.
    * unfold append. fold (@append A).
      rewrite IHxs. reflexivity.
  Qed.

  Lemma rev_app_distr {A: Type} : 
    forall x y: lst,
    rev (x +++ y) = ((rev y +++ rev x) : @lst A).
  Proof.
    intros x y.
    induction x.
    * simpl. rewrite app_right_neutral. reflexivity.
    * unfold append. fold (@append A).
      unfold rev. fold (@rev A).
      rewrite IHx. 
      rewrite -> app_assoc. (* rewrite <- is OK too *)
      reflexivity.
  Qed.

  
  Remark rev_unit : 
    forall {A: Type} (l: lst) (a: A),
      rev (l +++ [a]) = Cons a (rev l).
  Proof.
    intros A l a.
    induction l.
    * unfold rev. fold (@rev A). unfold append. unfold rev. unfold append.
      reflexivity.
    * simpl. rewrite  IHl. simpl. reflexivity.
  Qed.


  Lemma rev_involutive : forall {A: Type} (l: lst), @rev A (rev l) = l.
  Proof.
    intros. induction l.
    * unfold rev. reflexivity.
    * simpl. rewrite rev_app_distr. rewrite IHl. simpl. reflexivity.
  Qed.

Section ReverseWithAcc.
  Fixpoint reverse1 {A: Type} (acc xs: lst) {struct xs} : @lst A :=
    match xs with
    | Nil => acc
    | Cons h tl => reverse1  (@Cons A h acc) tl
    end.

  Definition reverse {A: Type} (xs: lst) : @lst A :=
    reverse1 [] xs.

  Lemma reverse1_unit {A: Type}: forall acc xs,
     reverse1 acc xs = @reverse1 A [] xs +++ acc.
  Proof.
    intros acc xs. revert acc.
    induction xs.
    * intros. simpl. reflexivity.
    * intros. unfold reverse1. fold @reverse1.
      rewrite (IHxs (Cons a acc)).
      rewrite (IHxs [a]).
      fold (@append A [a] acc).
      rewrite app_assoc.
      reflexivity.
    Qed.


  Theorem rev_equivalent_reverse {A: Type}: forall xs,
    rev xs = @reverse A xs.
  Proof.
    induction xs.
    * unfold rev. unfold reverse. unfold reverse1. reflexivity.
    * unfold rev. fold @rev. 
      rewrite -> IHxs.
      unfold reverse at 2. unfold reverse1. fold @reverse1.
      rewrite reverse1_unit. 
      unfold reverse.
      reflexivity.
  Qed.

  Lemma reverse_involutive : forall {A: Type} (l: lst),
    @reverse A (reverse l) = l.
  Proof.
    intros.
    rewrite <- (rev_equivalent_reverse l).
    rewrite <- (rev_equivalent_reverse (rev l)).
    rewrite rev_involutive.
    reflexivity.
  Qed.


  (* ДОказать в лоб у меня не получилось. *)
  (*
  Lemma reverse1_app_distrib {A: Type}: forall xs ys,
    reverse1 [] (xs +++ ys) = @reverse1 A (reverse xs) ys.
  Proof.
    intros xs ys. generalize dependent xs.  generalize dependent ys.
    induction ys.
    2: {
      intros xs. unfold reverse1.
      fold @reverse1.
      assert (Cons a  ys = [a] +++ ys).
        * unfold append. reflexivity.
      * rewrite H. rewrite <- app_assoc.
        rewrite (IHys (xs +++ [a])).
        generalize dependent xs.
        induction xs.
        + unfold append. unfold reverse. unfold reverse1. fold @reverse1. reflexivity.
        +




    induction xs.
    * intros. simpl. induction ys.
      + simpl. unfold reverse. unfold reverse1. reflexivity.
      + unfold reverse1. fold @reverse1.
        unfold reverse. unfold reverse1. fold @reverse1. reflexivity.
    * unfold reverse. unfold reverse1. fold @reverse1.
      intros ys.
      induction ys.
      + unfold reverse1. fold @reverse1. unfold append. fold @append.
        unfold reverse1. fold @reverse1. rewrite app_right_neutral. reflexivity.
      + unfold reverse1. fold @reverse1.
        unfold reverse1. fold @reverse1.
      unfold append. fold @append. unfold reverse1. fold @reverse1.
    Admitted.

  Lemma reverse_involutive : forall {A: Type} (l: lst),
    @reverse A (reverse l) = l.
  Proof.
    intros. induction l.
    * unfold reverse. simpl. reflexivity.
    * unfold reverse.
      unfold reverse1. fold (@reverse1 A). fold (@reverse1 A).
      unfold reverse1. fold (@reverse1 A).  fold (@reverse1 A).
          rewrite  (@reverse1_unit A [a]).
          fold (@reverse A l).
        rewrite (reverse1_app_distrib (reverse1 [] l)).
        fold (@reverse A l).
        rewrite IHl.
        unfold reverse1. reflexivity.
  Qed.
*)
End ReverseWithAcc.

Section Folds.
  Fixpoint foldl {A B: Type} (f: A -> B -> A) (i: A) (xs: lst) :=
    match xs with
    | Nil => i
    | Cons h tl => foldl f (f i h) tl
    end.

  Fixpoint foldr {A B: Type} (f: A -> B -> A) (i: A) (xs: lst) :=
    match xs with
    | Nil => i
    | Cons h tl => f (foldr f i tl) h
    end.

  Search "add".
  Require Import Nat.
  Compute foldl add 0 [1;2;3].
  Compute foldr add 0 [1;2;3].

  (* TODO: rewrite as defintion *)
  Definition func_is_commutative {A B: Type} (f: A -> A -> B) : Prop :=
    forall (a b: A),  f a b = f b a.

  Definition func_is_associative {A: Type} (f: A -> A -> A) :=
    forall (a b c: A),
        f a (f b c) = f (f a b) c.

  Lemma test1 {A: Type} (f: A -> A -> A):
    func_is_associative f ->
    func_is_commutative f -> forall (x y z: A) ,
      f x (f y z) = f z (f y x).
  Proof.
    intros.
    unfold func_is_commutative in H0.
    unfold func_is_associative in H.
    rewrite H; rewrite H.
    replace (f x y) with (f y x).
    replace (f z y) with (f y z).
    rewrite <- H. rewrite <- H.
    replace (f x z) with (f z x).
    reflexivity.
      rewrite (H0 x z). reflexivity.
      rewrite (H0 y z). reflexivity.
      rewrite (H0 x y). reflexivity.
  Qed.

  Theorem foldl_and_f_comm {A: Type} (f: A -> A -> A):
    func_is_associative f ->
    func_is_commutative f ->
    forall (a i: A) (xs: lst),
      foldl f (f i a) xs = f (foldl f i xs) a.
  Proof.
    intros assocf commf.
    intros. generalize dependent a. generalize dependent i.
    induction xs.
    * simpl. reflexivity.
    * simpl. 
      unfold func_is_associative in assocf.
      unfold func_is_commutative in commf.
      intros.
      rewrite (IHxs (f i a0) a).
      replace (f i a0) with (f a0 i).
      fold (@foldl A A f a0 (Cons i xs)).
      rewrite (IHxs a0 i).
      rewrite (IHxs i a).
      rewrite <- assocf.
      rewrite <- assocf.
      replace (f a a0) with (f a0 a).
      rewrite  assocf. rewrite  assocf.
      rewrite <- (IHxs a0 i).
      rewrite <- (IHxs (f a0 i) a).
      rewrite <- (IHxs).
      rewrite <- (IHxs (f i a0) ).
      rewrite <- assocf.
      rewrite (commf a0 (f i a)).
      rewrite <- assocf. rewrite <- assocf.
      rewrite (commf a0 a). reflexivity.
      rewrite commf. reflexivity.
      rewrite commf. reflexivity.
    Qed.

  Theorem foldr_and_f_comm {A: Type} (f: A -> A -> A):
    func_is_associative f ->
    func_is_commutative f ->
    forall (a i: A) (xs: lst),
      foldr f (f i a) xs = f (foldr f i xs) a.
 Proof.
    intros assocf commf.
    intros. generalize dependent a. generalize dependent i.
    induction xs.
    * simpl. reflexivity.
    * simpl. intros. rewrite (IHxs i a0).
      unfold func_is_associative in assocf.
      unfold func_is_commutative in commf.
      replace (f (f (foldr f i xs) a) a0)
         with (f (foldr f i xs)  (f a a0)).
      + rewrite <- assocf.
        replace (f a a0) with (f a0 a). reflexivity.
        apply commf.
      + rewrite assocf. reflexivity.
    Qed.
(*   Theorem foldl_app_disrib {A: Type}  (f: A -> A -> A):
    forall (i: A) (xs ys: lst),
      @foldl A A f i (xs +++ ys) = foldl f (foldl f i xs) ys.
  Proof. Admitted. *)
(*
  Theorem foldr_of_rev_when_comm {A: Type} (f: A -> A -> A):
    func_is_associative f ->
    func_is_commutative f -> forall (i: A) (xs: lst),
      foldl f i xs = foldl f i (rev xs).
  Proof.
    intros assocf commf.
    intros i xs. generalize dependent i.

    induction xs.
    * unfold rev.  reflexivity.
    * unfold foldl. fold @foldl.
      unfold rev. fold @rev.
      intros i.
      rewrite (foldl_app_disrib).
      simpl.
      rewrite <- (IHxs i).

      remember (@foldl_and_f_comm A f assocf commf a i xs).
      assumption.
  Qed.
*)
  Theorem foldl_is_foldr_when_comm {A: Type} (f: A -> A -> A):
    func_is_associative f ->
    func_is_commutative f -> forall (i: A) (xs: lst),
      foldl f i xs = foldr f i xs.
  Proof.
    intros assof commf.
    intros i xs. generalize dependent i.
    induction xs.
    * unfold foldl. unfold foldr. reflexivity.
    * unfold foldl. fold @foldl.
      intros i. rewrite (IHxs (f i a)).
      unfold foldr. fold @foldr.
      rewrite (@foldr_and_f_comm A f assof commf).
      reflexivity.
  Qed.

  (* https://stackoverflow.com/questions/67008673/foldr-using-foldl-on-finite-lists
    Proves definition of foldl using foldr *)
End Folds.


(* *********************** *)
Require Extraction.
Extraction Language OCaml.

Extract Inductive bool => "bool" [ "true" "false" ].
Extract Inductive lst => "list" [ "[]" "(::)" ].
Extraction "Lists.ml"  append rev reverse foldl foldr.



