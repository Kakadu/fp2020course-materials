module SlowId = struct
  type 'a t = 'a

  let ( <|> ) a _ =
    (* print_endline "Sleeping"; *)
    Thread.delay 2.0;
    print_endline "something calculated";
    a
  ;;

  let pure x = x
  let fmap f x = f x
  let ( >>| ) xs f = fmap f xs
  let ( <*> ) : 'a 'b. ('a -> 'b) t -> 'a t -> 'b t = fun fs xs -> fs xs

  module Syntax = struct
    let ( let+ ) = ( >>| )
  end
end

module ConcurrentId = struct
  type 'a t = 'a Lwt.t

  open Lwt
  open Lwt.Syntax

  let ( <|> ) a _ = a

  let pure x =
    let* () = Lwt_unix.sleep 2. in
    print_endline "something calculated";
    return x
  ;;

  let fmap = Lwt.map
  let ( >>| ) xs f = fmap f xs

  let ( <*> ) : 'a 'b. ('a -> 'b) t -> 'a t -> 'b t =
   fun fs xs ->
    let* f, x = Lwt.both fs xs in
    return (f x)
 ;;

  module Syntax = struct
    let ( let+ ) = ( >>| )
  end
end

open Expr0

let () =
  print_endline "Slow mode";
  let module E = Eval (SlowId) in
  E.eval (Plus (Const 1, Const 2)) |> ignore
;;

let () =
  print_endline "Concurrent mode";
  let module E = Eval (ConcurrentId) in
  E.eval (Plus (Const 1, Const 2)) |> Lwt_main.run |> print_int
;;
