(** Copyright 2021-2023, Kakadu *)

(** SPDX-License-Identifier: LGPL-3.0-or-later *)

(* errors via Result (by hand) *)

type expr =
  | Const of int
  | Plus of expr * expr
  | Slash of expr * expr
  | Asterisk of expr * expr
  | Var of string

(* TODO: use polymorphic variants *)
type error = Division_by_zero | Var_unbound of string

let pp_error ppf = function
  | Division_by_zero ->
      Format.fprintf ppf "Division_by_zero"
  | Var_unbound s -> Format.fprintf ppf "Unbound var `%s`" s

let eval from_env =
  let ( let* ) x f =
    match x with
    | Ok x -> f x
    | Result.Error e -> Result.Error e
  in
  let return s = Result.Ok s in
  let fail s = Result.Error s in
  let rec helper = function
    | Const n -> return n
    | Plus (l, r) ->
        let* l = helper l in
        let* r = helper r in
        return (l + r)
    | Asterisk (l, r) ->
        let* l = helper l in
        let* r = helper r in
        return (l * r)
    | Slash (l, r) ->
        let* r = helper r in
        if r = 0 then fail Division_by_zero
        else
          let* l = helper l in
          return (l / r)
    | Var s -> from_env s
  in
  helper

let run_and_pp x =
  let pp_result arg ppf = function
    | Result.Error e ->
        Format.fprintf ppf "Error (%a)" pp_error e
    | Ok x -> Format.fprintf ppf "%a" arg x
  in
  Format.printf "%a\n%!" (pp_result Format.pp_print_int) x

let%expect_test _ =
  run_and_pp
  @@ eval
       (fun s -> Error (Var_unbound s))
       (Plus (Const 1, Asterisk (Const 2, Const 3)));
  [%expect {| 7 |}]

let%expect_test _ =
  run_and_pp
  @@ eval (fun s -> Error (Var_unbound s)) (Var "x");
  [%expect {| Error (Unbound var `x`) |}]

let%expect_test _ =
  run_and_pp
  @@ eval
       (fun s -> Error (Var_unbound s))
       (Slash (Const 1, Const 0));
  [%expect {| Error (Division_by_zero) |}]
