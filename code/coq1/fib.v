Search nat.
Fixpoint fib n :=
  match n with
  | O => O
  | S O => S O
  | S ((S p) as p1) => (fib p) + fib p1
  end.

Compute  fib 5.
Compute  fib 6.
Compute  fib 7.


Fixpoint fac n :=
  match n with
  | O => S O
  | S p => n * (fac p) end.

Eval compute in  (fac 6 = 720).

(* TODO: https://github.com/math-comp/Coq-Combi *)
(* https://cs.pomona.edu/~michael/courses/csci054s18/book/Combo.html *)
Fixpoint choose (n m : nat) : nat :=
  match n, m with
  | _, O => 1
  | O, S m' => 0
  | S n', S m' => choose n' (S m') + choose n' m'
  end.

  Lemma choose_n_0 : forall n : nat, choose n 0 = 1.
Proof.
  (* FILL IN HERE *) Admitted.
Lemma choose_n_lt_m : forall n m : nat,
    n < m -> choose n m = 0.
Proof.
  (* FILL IN HERE *) Admitted.
Lemma choose_n_n : forall n : nat, choose n n = 1.
Proof.
  (* FILL IN HERE *) Admitted.
