
Theorem id1 : forall  (A: Type) , A -> A.
Proof.
  intros A. intros x. assumption.
Qed.


Definition id2 : forall  (A: Type) , A -> A.
Proof.
  refine (fun _ x => x).
Qed.

Print id1.
Print id2.

Theorem cut_elim1 : forall (A B: Type),
  A -> (A -> B) -> B.
Proof.
  intros A B. intros x f. apply f. assumption.
Qed.

Definition cut_elim2 : forall (A B: Type),
  A -> (A -> B) -> B.
Proof.
  refine (fun _ _ x f => f x).
Qed.


Definition double_negation1 : forall A: Type,
  A -> (A -> False) -> False.
Proof.
  intros A. intros X. intros H. apply H in X. assumption.
Qed.
Print double_negation1.
Definition double_negation2 : forall A: Type,
  A -> ((A -> False) -> False).
Proof.
  refine (fun _ x f => f x).
Qed.

Print double_negation1.
Print double_negation2.
Print cut_elim2.
(* *********************** *)
Require Extraction.
Extraction Language OCaml.

Extraction cut_elim2.
Extraction double_negation2.