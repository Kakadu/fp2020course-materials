(** Copyright 2021-2023, Kakadu *)

(** SPDX-License-Identifier: LGPL-3.0-or-later *)

(* Applicative from Monad *)

type expr =
  | Const of int
  | Plus of expr * expr
  | Slash of expr * expr
  | Asterisk of expr * expr
  | Var of string

module type MONAD = sig
  type 'a t

  val return : 'a -> 'a t
  val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t

  module Syntax : sig
    val ( let* ) : 'a t -> ('a -> 'b t) -> 'b t
  end
end

module type MONADERROR = sig
  include MONAD

  val fail : string -> 'a t
end

module type APPLICATIVE = sig
  type 'a t

  val pure : 'a -> 'a t
  val ( <*> ) : ('a -> 'b) t -> 'a t -> 'b t
end

module ApplicativeOfMonad (M : MONAD) :
  APPLICATIVE with type 'a t = 'a M.t = struct
  include M

  let pure = return

  let ( <*> ) : ('a -> 'b) M.t -> 'a M.t -> 'b M.t =
   fun f x ->
    f >>= fun f ->
    x >>= fun x -> return (f x)
end

module Eval (M : MONADERROR) = struct
  open M
  open ApplicativeOfMonad (M)

  let eval from_env : expr -> int M.t =
    let rec helper = function
      | Const n -> return n
      | Plus (l, r) ->
          return ( + ) <*> helper l <*> helper r
      | Asterisk (l, r) ->
          return ( * ) <*> helper l <*> helper r
      | Slash (l, r) -> (
          return (fun l r -> (l, r))
          <*> helper l <*> helper r
          >>= function
          | _, 0 -> fail "division by zero"
          | l, r -> return (l / r))
      | Var s -> from_env s
    in
    helper
end

module Option = struct
  type 'a t = 'a option

  let ( >>= ) = Option.bind
  let return = Option.some
  let fail _ = None

  module Syntax = struct
    let ( let* ) = ( >>= )
  end
end

let%test _ =
  let module E = Eval (Option) in
  Some 7
  = E.eval
      (fun _ -> None)
      (Plus (Const 1, Asterisk (Const 2, Const 3)))
