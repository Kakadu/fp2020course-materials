
Fixpoint factorial (n:nat) : nat :=
  match n with
    | 0 => 1
    | S n' => n * factorial n'
  end.
Example fact_0__1 : factorial 0 = 1.
Proof. reflexivity. Qed.
Example fact_5__120 : factorial 5 = 120.
Proof. reflexivity. Qed.

Lemma factorial_nz : forall n : nat,
    factorial n <> 0.
Proof.
  intros n.
  induction n.
  * unfold factorial. discriminate.
  * unfold factorial. fold factorial.
  unfold not.
  Require Import PeanoNat.
  Search (_ <> _) "mul". intros H.
  (* TODO: можно ли это доказать без Nat.mul_eq_0_l ? *)
  remember (Nat.mul_eq_0_l (S n) (factorial n) H IHn).
  inversion e.
Qed.


(* *** *)

Require Import List.
Import ListNotations.

Fixpoint everywhere {A:Type} (a:A) (ls:list A) : list (list A) :=
  match ls with
  | [] => [[a]]
  | h :: t => (a :: ls) :: (map (fun t' => h::t') (everywhere a t))
  end.
Example everywhere_1_234 :
  everywhere 1 [2;3;4] = [[1;2;3;4];
                          [2;1;3;4];
                          [2;3;1;4];
                          [2;3;4;1]].
Proof. reflexivity. Qed.

Fixpoint concat_map {A B:Type} (f:A -> list B) (l:list A) : list B :=
  match l with
  | [] => []
  | a :: l' => f a ++ concat_map f l'
  end.
Fixpoint permutations {A:Type} (ls:list A) : list (list A) :=
  match ls with
  | [] => [[]] | h :: t => concat_map (everywhere h) (permutations t)
  end.
Compute (permutations [1;2;3;4]).
Example permutations_1234 :
  permutations [1;2;3;4] =
  [(* insert 1 everywhere into 2;3;4 *)
   [1; 2; 3; 4];
   [2; 1; 3; 4];
   [2; 3; 1; 4];
   [2; 3; 4; 1];

   (* insert 1 everywhere into 3;2;4 *)
   [1; 3; 2; 4];
   [3; 1; 2; 4];
   [3; 2; 1; 4];
   [3; 2; 4; 1];

   (* insert 1 everywhere into 3;4;2 *)
   [1; 3; 4; 2];
   [3; 1; 4; 2];
   [3; 4; 1; 2];
   [3; 4; 2; 1];

   (* insert 1 everywhere into 2;4;3 *)
   [1; 2; 4; 3];
   [2; 1; 4; 3];
   [2; 4; 1; 3];
   [2; 4; 3; 1];

   (* insert 1 everywhere into 4;2;3 *)
   [1; 4; 2; 3];
   [4; 1; 2; 3];
   [4; 2; 1; 3];
   [4; 2; 3; 1];

   (* insert 1 everywhere into 4;3;2 *)
   [1; 4; 3; 2];
   [4; 1; 3; 2];
   [4; 3; 1; 2];
   [4; 3; 2; 1]].
Proof. reflexivity. Qed.
Compute length (everywhere 5 [1;2;3;4]).

Theorem exists_example_2 : forall n,
  (exists m, n = 4 + m) ->
  (exists o, n = 2 + o).
Proof.
  (* WORKED IN CLASS *)
  intros n G. destruct G as [m Hm]. (* note implicit destruct here *)
  exists (2 + m).
  apply Hm. Qed.

Lemma In_concat_map:
  forall (A B : Type) (f : A -> list B) (l : list A) (y : B),
  In y (concat_map f l) -> (exists x : A, In y (f x) /\ In x l).
Proof.
  intros A B f.
  induction l.
  * simpl. intros. contradiction.
  * intros. unfold concat_map in H. fold @concat_map in H.
    exists a. split.
    2: { unfold In. left. reflexivity. }
    Search "In".
    assert (forall a xs ys, In a (xs++ys) -> In a xs \/ @In A a ys).
    + induction xs.
      ++simpl. intros. right. assumption.
      ++ intros ys. unfold app. fold (@app A xs ys).
         intros.
         (* unfold In in H0. *)
         destruct H0.
         left; replace a0 with a1. unfold In; left; auto.
         remember (IHxs ys H0). destruct o.
         left; reflexivity. left; reflexivity. fold @In in H0.
      Print "_ ++ _".
      unfold . fold In.
    unfold In.


  split.

  (* FILL IN HERE *) Admitted.
