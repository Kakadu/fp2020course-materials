  $ cat <<EOF | ./Prover.exe 'a=>a'
  > Intros.
  > Axiom.
  > Qed.
  > EOF
  : not implemented

  $ cat <<EOF | ./Prover.exe 'a=>b=>a'
  > Intros.
  > Intros.
  > Axiom.
  > Qed.
  > EOF
  : not implemented
