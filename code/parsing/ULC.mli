module Lam : sig
  type t = Var of char | App of t * t | Abs of char * t [@@deriving show]

  val var : char -> t
  val abs : char -> t -> t
  val app : t -> t -> t
end

type dispatch = {
  apps : dispatch -> Lam.t Angstrom.t;
  single : dispatch -> Lam.t Angstrom.t;
}

val parse_lam : dispatch
val parse_optimistically : string -> Lam.t

module PP : sig
  val pp : Format.formatter -> Lam.t -> unit
end

module TestQCheck : sig
  val lam_gen_manual : Lam.t QCheck.Gen.t
  val run_manual : unit -> int
  val run_auto : unit -> int
end
