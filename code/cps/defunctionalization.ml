(* Don't show tomorrow

   (* https://discuss.ocaml.org/t/a-y-combinator-you-can-actually-use/10886/9?u=kakadu *)

   (* let%test _ = false *)

   module Attempt1 = struct
     let rec fac n = if n <= 1 then 1 else n * fac (n - 1)
   end

   module Attempt2 = struct
     let rec fack n k =
       if n <= 1 then k 1 else fack (n - 1) (fun prev -> k (n * prev))
   end

   (* TODO(Kakadu): Concpet of closure, defunctionalization, OOP *)
   (* module Attempt3 = struct *)
   type kont = K0 | K1 of { x : int; k : kont }

   let rec recover = function
     | K0 -> fun acc -> acc
     | K1 { x; k } -> fun acc -> (recover k) (x * acc)

   let rec facD x k = if x <= 1 then recover k 1 else facD (x - 1) (K1 { x; k })
   let fac x = facD x K0
   (* end *)

   module Fib = struct
     let rec fibk n k =
       if n = 0 then k 0
       else if n = 1 then k 1
       else fibk (n - 1) (fun p -> fibk (n - 2) (fun q -> k (p + q)))

     type kont = K0 | K1 | KNode of kont * kont

     let rec recover : kont -> int  -> int = function
       | K0 -> fun n -> 0
       | K1 -> fun n -> 1
       | KNode (l, r) ->
           fun (p) ->
             let l = recover l in
             let r = recover r
             Format.printf "p=%d q=%d\n%!" p q;
             let l = recover l (p, q) in
             recover r (q, q + l)

     let rec fibD n k1 k2 =
       if n = 0 then recover k1 (0, 1)
       else if n = 1 then recover k2 (1, 1)
       else fibD (n - 1) k2 (KNode (k1, k2))

     let fib n = fibD n K0 K1;;
     #trace fibD;;

   end

   let%test _ = Fib.fib 0 = 0
   let%test _ = Fib.fib 1 = 1
   let%test _ = Fib.fib 2 = 1
   let%test _ = Fib.fib 3 = 2
   let%test _ = Fib.fib 4 = 3 *)
