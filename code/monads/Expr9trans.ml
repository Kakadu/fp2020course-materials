(* State/Reader/Writer *)
(* TODO: сказать когда какая монада нужна *)

type expr =
  | Const of int
  | Plus of expr * expr
  | Asterisk of expr * expr
  | Var of string

(* Indexed monad. *)
module type MONAD = sig
  type ('s, 'a) t

  val return : 'a -> ('s, 'a) t

  val ( >>= ) :
    ('s, 'a) t -> ('a -> ('s, 'b) t) -> ('s, 'b) t

  module Syntax : sig
    (* A synonym for >>= *)
    val ( let* ) :
      ('s, 'a) t -> ('a -> ('s, 'b) t) -> ('s, 'b) t
  end
end

module type MONAD_ERROR = sig
  include MONAD

  val error : string -> (_, 'a) t
end

module Eval1 (M : MONAD_ERROR) = struct
  open M
  open M.Syntax

  let eval :
      (string -> (string, int) M.t) ->
      expr ->
      (string, int) M.t =
   fun from_env ->
    let rec helper = function
      | Const n -> return n
      | Plus (l, r) ->
          let* l = helper l in
          let* r = helper r in
          return (l + r)
      | Asterisk (l, r) ->
          let* l = helper l in
          let* r = helper r in
          return (l * r)
      | Var s -> from_env s
    in
    helper
end

module type WRITER_MONAD = sig
  include MONAD

  val write : 's -> ('s, unit) t
  val run : ('st, 'a) t -> 'st list * 'a
end

(* A la logger *)
module Simple_writer : WRITER_MONAD = struct
  type ('st, 'a) t = 'st list * 'a

  let ( >>= ) :
        's 'a 'b.
        ('s, 'a) t -> ('a -> ('s, 'b) t) -> ('s, 'b) t =
   fun (log1, x) f : _ ->
    let log2, rez = f x in
    (log1 @ log2, rez)

  let return : 'a -> ('st, 'a) t = fun x -> ([], x)
  let write : 'st -> ('st, unit) t = fun x -> ([ x ], ())
  let run : ('st, 'a) t -> 'st list * 'a = Fun.id

  module Syntax = struct
    let ( let* ) = ( >>= )
  end
end

module Eval3 (M : WRITER_MONAD) = struct
  open M
  open M.Syntax

  let eval : expr -> (string, int) M.t =
    let rec helper = function
      | Const n -> return n
      | Plus (l, r) ->
          let* l = helper l in
          let* r = helper r in
          let* () = write "Addition" in
          return (l + r)
      | Asterisk (l, r) ->
          let* l = helper l in
          let* r = helper r in
          let* () = write "Multiplication" in
          return (l * r)
      | Var _ -> failwith "not important for this example"
    in
    helper
end

let%test _ =
  let module E = Eval3 (Simple_writer) in
  let expr = Plus (Const 1, Asterisk (Const 2, Const 3)) in
  match Simple_writer.run (E.eval expr) with
  | text, rez ->
      text = [ "Multiplication"; "Addition" ] && rez = 7

module type WRITER_AND_ERROR = sig
  include MONAD_ERROR
  include WRITER_MONAD with type ('e, 'a) t := ('e, 'a) t

  val run : ('st, 'a) t -> 'st list * 'a option
end

module Writer_after_error : WRITER_AND_ERROR = struct
  type ('log, 'a) t = 'log list -> 'log list * 'a option

  let write : 'log. 'log -> ('log, unit) t =
   fun x xs -> (x :: xs, Some ())

  let error _ : _ = fun st -> (st, None)
  let return : 'a -> ('st, 'a) t = fun x xs -> (xs, Some x)

  let run : ('st, 'a) t -> 'st list * 'a option =
   fun m -> m []

  [@@@ocaml.warnerror "-27"]

  let ( >>= ) :
        's 'a 'b.
        ('s, 'a) t -> ('a -> ('s, 'b) t) -> ('s, 'b) t =
   fun m f : _ ->
    fun st ->
     match m st with
     | log1, None -> (log1, None)
     | log1, Some rez -> (
         match f rez log1 with log2, x -> (log2, x))

  module Syntax = struct
    let ( let* ) = ( >>= )
  end
end

module Eval4 (M : WRITER_AND_ERROR) = struct
  open M
  open M.Syntax

  let eval : expr -> (string, int) M.t =
    let rec helper = function
      | Const n -> return n
      | Plus (l, r) ->
          let* l = helper l in
          let* r = helper r in
          let* () = write "Addition" in
          return (l + r)
      | Asterisk (l, r) ->
          let* l = helper l in
          let* r = helper r in
          let* () = write "Multiplication" in
          return (l * r)
      | Var _ -> error "not important for this example"
    in
    helper
end

let%test _ =
  let module E = Eval4 (Writer_after_error) in
  let expr =
    Plus
      (Plus (Asterisk (Const 2, Const 3), Const 1), Var "x")
  in
  match Writer_after_error.run (E.eval expr) with
  | text, rez ->
      (* List.iter print_endline text; *)
      text = [ "Addition"; "Multiplication" ] && rez = None
(*
module Error_after_writer : WRITER_AND_ERROR = struct
  type ('log, 'a) t = ('log list -> 'log list * 'a) option

  let write : 'log. 'log -> ('log, unit) t =
   fun x -> Some (fun xs -> (x :: xs, ()))

  let error _ : (_, _) t = None

  let return : 'a -> ('st, 'a) t =
   fun x -> Some (fun xs -> (xs, x))

  let run : ('st, 'a) t -> ('st list * 'a) option =
   fun m -> Option.map (fun f -> f []) m

  [@@@ocaml.warnerror "-27"]

  let ( >>= ) :
        's 'a 'b.
        ('s, 'a) t -> ('a -> ('s, 'b) t) -> ('s, 'b) t
      (* 's.
         ('s, int) t ->
         (int -> ('s, string) t) ->
         ('s, string) t *) =
   fun m f : _ ->
    match m with
    | None -> None
    | Some rez ->
        Some
          (fun st ->
            match f (rez st) with log2, x -> (log2, x))

  module Syntax = struct
    let ( let* ) = ( >>= )
  end
end
 *)
