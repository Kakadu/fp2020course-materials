

(* Require Import Int. *)
Require Import List.
Import ListNotations.
Require Import Coq.Numbers.BinNums. (* for Z *)

(* Parameter int : Type. *)
Parameter Abs : nat -> Z.
Axiom Abs_inj: forall (n m : nat), Abs n = Abs m -> n = m.

Parameter ltb: nat -> nat -> bool.

(* Extract Inlined Constant int ⇒ "int". *)
Definition key := nat.

Inductive color := Red | Black.

Inductive tree {A: Type} : Type :=
| E : tree
| T : color -> tree -> key -> A -> tree -> tree.

Definition empty_tree (A: Type): @tree A := E.

Fixpoint lookup {A: Type} (x: key) (t : tree ) : option A :=
  match t with
  | E => None
  | T _ tl k v tr => if ltb x k then lookup x tl
                    else if ltb k x then lookup x tr
                          else v
  end.


  Definition balance  {A: Type} (rb : color) (t1 : tree) (k : key) (vk : A) (t2 : tree) : tree :=
    match rb with
    | Red => T Red t1 k vk t2
    | _ => match t1 with
          | T Red (T Red a x vx b) y vy c =>
            T Red (T Black a x vx b) y vy (T Black c k vk t2)
          | T Red a x vx (T Red b y vy c) =>
            T Red (T Black a x vx b) y vy (T Black c k vk t2)
          | _ => match t2 with
                | T Red (T Red b y vy c) z vz d =>
	          T Red (T Black t1 k vk b) y vy (T Black c z vz d)
                | T Red b y vy (T Red c z vz d)  =>
	          T Red (T Black t1 k vk b) y vy (T Black c z vz d)
                | _ => T Black t1 k vk t2
                end
          end
    end.

  Fixpoint ins {A: Type} (x : key) (vx : A) (t : tree) : tree :=
    match t with
    | E => T Red E x vx E
    | T c a y vy b => if ltb x y then balance c (ins x vx a) y vy b
                      else if ltb y x then balance c a y vy (ins x vx b)
                           else T c a x vx b
    end.

  Definition make_black {A: Type} (t : tree) : @tree A :=
    match t with
    | E => E
    | T _ a x vx b => T Black a x vx b
    end.

  Definition insert {A: Type} (x : key) (vx : A) (t : tree) :=
    make_black (ins x vx t).

  (** The [elements] implementation is the same as for BSTs, except that it
      ignores colors. *)


  Fixpoint elements_tr {A: Type} (t : tree) (acc: list (key * A)) : list (key * A) :=
    match t with
    | E => acc
    | T _ l k v r => elements_tr l ((k, v) :: elements_tr r acc)
    end.

  Definition elements {A: Type}  (t : tree) : list (key * A) :=
    elements_tr t [].

(* ################################################################# *)
(** * Performance of Extracted Code *)

(** We can extract the red-black tree implementation: *)

Require Import Extraction.
Extract Inlined Constant ltb => "(-)".
Extract Constant ltb => "( < )".

Extract Inductive nat => int [ "0" "Pervasives.succ" ]
 "(fun fO fS n -> if n=0 then fO () else fS (n-1))".
Extract Inductive bool => "bool" [ "true" "false" ].
Extract Inductive list => "list" [ "[]" "(::)" ].
Extraction "redblack.ml" empty_tree insert lookup elements.

(** Run it in the OCaml top level with these commands:

      #use "redblack.ml";;
      #use "test_searchtree.ml";;

    On a recent machine with a 2.9 GHz Intel Core i9 that prints:

      Insert and lookup 1000000 random integers in 0.860663 seconds.
      Insert and lookup 20000 random integers in 0.007908 seconds.
      Insert and lookup 20000 consecutive integers in 0.004668 seconds.

    That execution uses the bytecode interpreter.  The native compiler
    will have better performance:

      $ ocamlopt -c redblack.mli redblack.ml
      $ ocamlopt redblack.cmx -open Redblack test_searchtree.ml -o test_redblack
      $ ./test_redblack

On the same machine that prints,

      Insert and lookup 1000000 random integers in 0.475669 seconds.
      Insert and lookup 20000 random integers in 0.00312 seconds.
      Insert and lookup 20000 consecutive integers in 0.001183 seconds.
*)

(** The benchmark measurements above (and in [Extract])
    demonstrate the following:

    - On random insertions, red-black trees are about the same as
      ordinary BSTs.

    - On consecutive insertions, red-black trees are _much_ faster
      than ordinary BSTs.

    - Red-black trees are about as fast on consecutive insertions as
      on random. *)

(* 2021-08-11 15:15 *)
