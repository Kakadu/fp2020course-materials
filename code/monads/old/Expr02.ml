(* interpreter with environment *)

(* Base case *)
type expr =
  | Const of int
  | Plus of expr * expr
  | Asterisk of expr * expr
  | Var of string

let rec eval lookup = function
  | Const n -> n
  | Plus (l, r) -> eval lookup l + eval lookup r
  | Asterisk (l, r) -> eval lookup l * eval lookup r
  | Var s -> lookup s

let%test _ =
  7
  = eval
      (fun _ -> assert false)
      (Plus (Const 1, Asterisk (Const 2, Const 3)))

let%test _ =
  3
  = eval
      (fun s -> List.assoc s [ ("x", 2) ])
      (Plus (Const 1, Var "x"))

module type APPLICATIVE = sig
  type 'a t

  val pure : 'a -> 'a t
  (** Same as return *)

  val ( <*> ) : ('a -> 'b) t -> 'a t -> 'b t

  (*
      val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t

      *)
  val ( >>| ) : 'a t -> ('a -> 'b) -> 'b t

  module Syntax : sig
    val ( let+ ) : 'a t -> ('a -> 'b) -> 'b t
    (* val ( and+ ) : 'a t -> 'b t -> ('a * 'b) t *)
  end
end

module Eval (M : APPLICATIVE) = struct
  open M

  let rec eval lookup = function
    | Const n -> pure n
    | Plus (l, r) ->
        pure ( + ) <*> eval lookup l <*> eval lookup r
    (*
        let* l = eval l in
        let* l = eval l in
        return (l*r)
         *)
    | Asterisk (l, r) ->
        pure ( + ) <*> eval lookup l <*> eval lookup r
    | Var s -> lookup s
end

module Id = struct
  type 'a t = 'a

  let ( <|> ) a _ = a
  let pure x = x
  let fmap f x = f x
  let ( >>| ) xs f = fmap f xs

  let ( <*> ) : 'a 'b. ('a -> 'b) t -> 'a t -> 'b t =
   fun fs xs -> fs xs

  module Syntax = struct
    let ( let+ ) = ( >>| )
  end
end

module _ = struct
  module E = Eval (Id)
  open E

  let () =
    assert (1 = eval (fun _ -> assert false) (Const 1))

  let () =
    assert (
      2
      = eval
          (fun _ -> assert false)
          (Plus (Const 1, Const 1)))

  let () =
    assert (
      3
      = eval
          (fun _ -> assert false)
          (Plus (Const 1, Const 2)))
end

module SlowId = struct
  type 'a t = 'a

  let ( <|> ) a _ =
    (* print_endline "Sleeping"; *)
    (* Thread.delay 2.0; *)
    (* print_endline "something calculated"; *)
    a

  let pure x = x
  let fmap f x = f x
  let ( >>| ) xs f = fmap f xs

  let ( <*> ) : 'a 'b. ('a -> 'b) t -> 'a t -> 'b t =
   fun fs xs -> fs xs

  module Syntax = struct
    let ( let+ ) = ( >>| )
  end
end

module ConcurrentId = struct
  type 'a t = 'a Lwt.t

  open Lwt
  open Lwt.Syntax

  let ( <|> ) a _ = a
  let pure x = return x
  let fmap = Lwt.map
  let ( >>| ) xs f = fmap f xs

  let ( <*> ) : 'a 'b. ('a -> 'b) t -> 'a t -> 'b t =
   fun fs xs ->
    let* f, x = Lwt.both fs xs in
    return (f x)

  module Syntax = struct
    let ( let+ ) = ( >>| )
  end
end
